/*
  RocDisplayBootloader.c
 
	� Walter Sax 2015
	
	V1.11
	
	Modified TWIBoot from Olaf Rempel with Intel Hex file paarser.
	
   This program is free software; you can redistribute it and/or modify  
   it under the terms of the GNU General Public License as published by  
   the Free Software Foundation; version 2 of the License,               
                                                                         
   This program is distributed in the hope that it will be useful,       
   but WITHOUT ANY WARRANTY; without even the implied warranty of        
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
   GNU General Public License for more details.                          
                                                                         
   You should have received a copy of the GNU General Public License     
   along with this program; if not, write to the                         
   Free Software Foundation, Inc.,                                       
   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
	
	
	See copyright and Licence from Olaf Rempel original version.
*/
/***************************************************************************
 *   Copyright (C) 08/2010 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; version 2 of the License,               *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

 /*
 RocDisplay:
 * Fuse E: 0xfd (2.7V BOD)
 * Fuse H: 0xd1 (2048 words bootloader)  / 0xd0 (2048 words w.o. bootloader after Reset)
 * Fuse L: 0xdf (16Mhz external RC-Osz.)
*/

//#define debug

#define F_CPU 16000000UL
#ifdef debug
	#define BAUD 2400UL			
#endif
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/boot.h>
#include <avr/pgmspace.h>
#include <string.h>
#include <util/twi.h>
#ifdef debug
	#include <util/setbaud.h>
	#include <stdio.h>
	#include <stdlib.h>
#endif // debug
#include <util/delay.h>




//ACK nach empfangenen Daten senden/ ACK nach gesendeten Daten erwarten
#define TWCR_ACK 	TWCR = (1<<TWEN)|(1<<TWIE)|(1<<TWINT)|(1<<TWEA)|(0<<TWSTA)|(0<<TWSTO)|(0<<TWWC);

//NACK nach empfangenen Daten senden/ NACK nach gesendeten Daten erwarten
#define TWCR_NACK 	TWCR = (1<<TWEN)|(1<<TWIE)|(1<<TWINT)|(0<<TWEA)|(0<<TWSTA)|(0<<TWSTO)|(0<<TWWC);

//switched to the non adressed slave mode...
#define TWCR_RESET 	TWCR = (1<<TWEN)|(1<<TWIE)|(1<<TWINT)|(1<<TWEA)|(0<<TWSTA)|(0<<TWSTO)|(0<<TWWC);

/* Zust�nde des Bootloader-Programms */
#define BOOT_STATE_EXIT	        0
#define BOOT_STATE_PARSER       1

/* Zust�nde des Hex-File-Parsers */
#define PARSER_STATE_START      0
#define PARSER_STATE_SIZE       1
#define PARSER_STATE_ADDRESS    2
#define PARSER_STATE_TYPE       3
#define PARSER_STATE_DATA       4
#define PARSER_STATE_CHECKSUM   5
#define PARSER_STATE_ERROR      6

#define START_SIGN              ':'      /* Hex-Datei Zeilenstartzeichen */

#define VERSION_STRING		"TWI_RN_Display Bootloader m328p v1.11"
#define SIGNATURE_BYTES		 SIGNATURE_0, SIGNATURE_1, SIGNATURE_2
#define BOOTLOADER_START 0x7000						// 2048 Words Bootloader

/* 192ms @16MHz */
#define TIMER_RELOAD		(0xF447)

/* 30 * 192ms =5,76s @16Mhz */
#define TIMEOUT			30L

#define LED_INIT()		DDRB = ((1<<PORTB1) | (1<<PORTB2))
#define LED_RT_ON()		PORTB |= (1<<PORTB1)
#define LED_RT_OFF()		PORTB &= ~(1<<PORTB1)
#define LED_GN_ON()		PORTB |= (1<<PORTB2)
#define LED_GN_OFF()		PORTB &= ~(1<<PORTB2)
#define LED_GN_TOGGLE()		PORTB ^= (1<<PORTB2)
#define LED_OFF()		PORTB = 0x00

#define ADR_INIT		DDRC &= ~((1<<PORTC0) || (1<<PORTC1) || (1<<PORTC2) || (1<<PORTC3))
#define Adr_PullUp		PORTC |= (1<<PORTC0) || (1<<PORTC1) || (1<<PORTC2) || (1<<PORTC3)
#define Read_Addr		(0x50 || (PINC & 15))
//#define Read_Addr()		0x02

/* SLA+R */
#define CMD_WAIT		0x00
#define CMD_READ_VERSION	0x01
#define CMD_READ_MEMORY		0x02
/* internal mappings */
#define CMD_READ_CHIPINFO	0x12	//	(0x10 | CMD_READ_MEMORY)
#define CMD_READ_FLASH		0x22	//	(0x20 | CMD_READ_MEMORY)
#define CMD_READ_EEPROM		0x32	//		(0x30 | CMD_READ_MEMORY)
#define CMD_READ_PARAMETERS	0x42	//	(0x40 | CMD_READ_MEMORY)	/* only in APP */

/* SLA+W */
#define CMD_SWITCH_APPLICATION	0x01
#define CMD_WRITE_MEMORY	0x02
/* internal mappings */
#define CMD_BOOT_BOOTLOADER		0x11	// (0x10 | CMD_SWITCH_APPLICATION)	/* only in APP */
#define CMD_BOOT_APPLICATION	0x21	//	(0x20 | CMD_SWITCH_APPLICATION)
#define CMD_WRITE_CHIPINFO		0x12	//	(0x10 | CMD_WRITE_MEMORY)	/* invalid */
#define CMD_WRITE_FLASH			0x22	//	(0x20 | CMD_WRITE_MEMORY)
#define CMD_WRITE_EEPROM		0x32	//	(0x30 | CMD_WRITE_MEMORY)
#define CMD_WRITE_PARAMETERS	0x42	//	(0x40 | CMD_WRITE_MEMORY)	/* only in APP */

/* CMD_SWITCH_APPLICATION parameter */
#define BOOTTYPE_BOOTLOADER	0x00				/* only in APP */
#define BOOTTYPE_APPLICATION	0x80

/* CMD_{READ|WRITE}_* parameter */
#define MEMTYPE_CHIPINFO	0x00
#define MEMTYPE_FLASH		0x01
#define MEMTYPE_EEPROM		0x02
#define MEMTYPE_PARAMETERS	0x03				/* only in APP */
#define MEMTYPE_ENDWRITE    0xFF

/*
 * LED_GN blinks with 20Hz (while bootloader is running)
 * LED_RT blinks on TWI activity
 *
 * bootloader twi-protocol:
 * - abort boot timeout:
 *   SLA+W, 0x00, STO
 *
 * - show bootloader version
 *   SLA+W, 0x01, SLA+R, {16 bytes}, STO
 *
 * - start application
 *   SLA+W, 0x01, 0x80, STO
 *
 * - read chip info: 3byte signature, 1byte page size, 2byte flash size, 2byte eeprom size
 *   SLA+W, 0x02, 0x00, 0x00, 0x00, SLA+R, {4 bytes}, STO
 *
 * - read one (or more) flash bytes
 *   SLA+W, 0x02, 0x01, addrh, addrl, SLA+R, {* bytes}, STO
 *
 * - read one (or more) eeprom bytes
 *   SLA+W, 0x02, 0x02, addrh, addrl, SLA+R, {* bytes}, STO
 *
 * - write one flash page (128bytes on mega328p)
	 SLA+W, 0x02, 0x01, HexFileLine, STO
	 
 *
 * - write one (or more) eeprom bytes
 *   SLA+W, 0x02, 0x02, addrh, addrl, {* bytes}, STO
 */
volatile uint8_t bcnt=0;
volatile uint8_t twiBuffer[64];


const static uint8_t info[40] = VERSION_STRING;
const static uint8_t chipinfo[14] = {
	SIGNATURE_0, ';',						//1,2
	SIGNATURE_1, ';',						//3,4
	SIGNATURE_2, ';',						//5,6
	SPM_PAGESIZE,';',						//7,8
	(BOOTLOADER_START >> 8) & 0xFF,			//9
	BOOTLOADER_START & 0xFF, ';',			//10,11
	((E2END +1) >> 8 & 0xFF),				//12
	(E2END +1) & 0xFF , 0x0c				//13,14
};


/* wait 40 * 25ms = 1s */
volatile uint16_t boot_timeout = TIMEOUT;
volatile uint8_t cmd = CMD_WAIT;
volatile uint8_t cmd1 = 0x00;

/* flash buffer */
volatile static uint8_t recCharF;
volatile static uint8_t GotChar;
volatile uint8_t regAddr = 0;
uint16_t addr=0;

#ifdef debug

void sendStr(char *command)
{
	uint8_t i=0;
	while(command[i]){          // *!='\0'* /
		while(!(UCSR0A&(1<<UDRE0)));
		UDR0=command[i];
		while(!(UCSR0A&(1<<UDRE0)));
		i++;
	}
}
void sendStr1(uint8_t *command)
{
	uint8_t i=0;
	while(command[i]){          // *!='\0'* /
		while(!(UCSR0A&(1<<UDRE0)));
		UDR0=command[i];
		while(!(UCSR0A&(1<<UDRE0)));
		i++;
	}
}
void sendByte(uint8_t command)
{
		while(!(UCSR0A&(1<<UDRE0)));
		UDR0=command;
		while(!(UCSR0A&(1<<UDRE0)));
	
}
#endif // debug

void program_page (uint32_t page, uint8_t *buf)
{
	LED_RT_ON();
	uint16_t i;
	uint8_t sreg;
	/* Disable interrupts */
	sreg = SREG;
	cli();
	
	eeprom_busy_wait ();
	
	boot_page_erase (page);
	boot_spm_busy_wait ();      /* Wait until the memory is erased. */
	
	for (i=0; i<SPM_PAGESIZE; i+=2)
	{
		/* Set up little-endian word. */
		uint16_t w = *buf++;
		w += (*buf++) << 8;
		
		boot_page_fill (page + i, w);
	}
	
	boot_page_write (page);     /* Store buffer in flash page.		*/
	boot_spm_busy_wait();       /* Wait until the memory is written.*/
	
	/* Reenable RWW-section again. We need this if we want to jump back */
	/* to the application after bootloading. */
	boot_rww_enable ();
	
	/* Re-enable interrupts (if they were ever enabled). */
	SREG = sreg;
}

uint16_t hex2num(uint8_t *s,uint8_t size)
{
	uint16_t x = 0;
	for(uint8_t z=0;z<size;z++) {
		char c = *s;
		if (c >= '0' && c <= '9') {
			x *= 16;
			x += c - '0';
		}
		else if (c >= 'A' && c <= 'F') {
			x *= 16;
			x += (c - 'A') + 10;
		}
		else break;
		s++;
	}
	return x;
}

/*
static uint8_t read_eeprom_byte(void)
{
	EEARL = addr;
	EEARH = (addr >> 8);
	EECR |= (1<<EERE);
	addr++;
	return EEDR;
}

static void write_eeprom_byte(uint8_t val)
{
	EEARL = addr;
	EEARH = (addr >> 8);
	EEDR = val;
	addr++;
	EECR |= (1<<EEMPE);
	EECR |= (1<<EEPE);
	eeprom_busy_wait();
}
*/
ISR (TWI_vect)
{
	uint8_t data;
	//LED_RT_ON();
	switch ((TW_STATUS)) 
	{
		/* SLA+W received, ACK returned -> receive data and ACK */
		case TW_SR_SLA_ACK:		// 0x60:
			LED_RT_ON();
			bcnt = 0;
			TWCR_ACK; 	
			break;

		/* prev. SLA+W, data received, ACK returned -> receive data and ACK */
		case TW_SR_DATA_ACK:			//0x80:
			
			data = TWDR;
			switch (bcnt) 
			{
				case 0:		// Register
					
					switch (data) 
					{
						case CMD_SWITCH_APPLICATION:
						case CMD_WRITE_MEMORY:
							bcnt++;
							/* no break */
						case CMD_WAIT:
							/* abort countdown */
							boot_timeout = 0;
							TWCR_ACK;
							break;

						default:
							/* boot app now */
							cmd = CMD_BOOT_APPLICATION;
							TWCR_ACK;
							break;
					}
					cmd = data;
					break;

				case 1:
					
					switch (cmd) 
					{
						case CMD_SWITCH_APPLICATION:
							if (data == BOOTTYPE_APPLICATION) 
							{
								cmd = CMD_BOOT_APPLICATION;
							}
							TWCR_ACK;
							break;

						case CMD_WRITE_MEMORY:
							
							switch (data)
							{
								case MEMTYPE_FLASH:
									cmd1 = CMD_WRITE_FLASH;
									recCharF=1;
									bcnt++;
								break;
								case MEMTYPE_CHIPINFO:
									cmd1 = CMD_WRITE_CHIPINFO;
									bcnt++;
								break;
								/*
								case MEMTYPE_EEPROM:
									cmd1 = CMD_WRITE_EEPROM;
									bcnt++;
								break;
								*/
								default:
									TWCR_NACK;
									break;
							}
							TWCR_ACK;
							break;

						default:
							bcnt++;
							TWCR_NACK;
							break;
					}
					break;
				case 2:
				case 3:
				
					if (cmd1==CMD_WRITE_FLASH)
					{
						
						twiBuffer[bcnt-2]=data;
						bcnt++;
						TWCR_ACK;
					}
					
					if ((cmd1==CMD_WRITE_CHIPINFO)||(cmd1==CMD_WRITE_EEPROM))
					{
						if ((bcnt == 2) & (data!=':'))
						{
							regAddr=1;
							addr=data;
							bcnt++;
							TWCR_ACK;	
						}
						else if ((bcnt == 3) & (regAddr==1))
						{
							regAddr=0;
							addr <<= 8;
							addr |= data;
							bcnt++;
							TWCR_ACK;
						}
					}
					break;

				default:
					if (cmd1==CMD_WRITE_FLASH) 
					{
							twiBuffer[bcnt-2]=data;
							bcnt++;
							TWCR_ACK;
					}
					/*
					else if (cmd1==CMD_WRITE_EEPROM)
					{
						case CMD_WRITE_EEPROM:
							write_eeprom_byte(data);
							bcnt++;
							TWCR_ACK;
					}
					*/
					else
					{
							TWCR_ACK;
					}
					break;
			}

			if (TWCR == ((1<<TWEN)|(1<<TWIE)|(1<<TWINT)|(0<<TWEA)|(0<<TWSTA)|(0<<TWSTO)|(0<<TWWC)))
			{
				
				//bcnt = 0;
			}
			//TWCR_ACK;
			break;

		/* SLA+R received, ACK returned -> send data */
		case TW_ST_SLA_ACK:			//0xA8:
			bcnt = 0;

		/* prev. SLA+R, data sent, ACK returned -> send data */
		case TW_ST_DATA_ACK:		//0xB8:
				if (cmd==CMD_READ_VERSION)
				{
					data = info[bcnt++];
					bcnt %= sizeof(info);
				}
				else if (cmd1==CMD_READ_CHIPINFO)
				{
					data = chipinfo[bcnt++];
					bcnt %= sizeof(chipinfo);
				}
				else if (cmd1==CMD_READ_FLASH)
				{
					data = pgm_read_byte_near(addr++);
				}
				/*
				else if (cmd1==CMD_READ_EEPROM)
				{
					data = read_eeprom_byte();
				}
				*/
				else
				{
					data = 0xFF;
				}

			TWDR = data;
			TWCR_ACK;
			break;
		
		case TW_ST_DATA_NACK: 						// 0xC0 Keine Daten mehr gefordert
		case TW_SR_DATA_NACK: 						// 0x88
		case TW_ST_LAST_DATA: 						// 0xC8  Last data byte in TWDR has been transmitted (TWEA = �0�); ACK has been received
		case TW_SR_STOP: 
		case TW_NO_INFO:							// 0xA0 STOP empfangen
		case TW_BUS_ERROR:
		default:
		if (recCharF==1)
		{
			GotChar=bcnt-2;
			recCharF=0;	
		}
		
		TWCR_RESET; 	
		bcnt = 0;						// �bertragung beenden, warten bis zur n�chsten Adressierung
		LED_RT_OFF();
		break;
	}
}


ISR(TIMER1_OVF_vect)
{
	/* restart timer */
	TCNT1 = TIMER_RELOAD;

	/* blink LED while running */
	LED_GN_TOGGLE();
	
	/* count down for app-boot */
	if (boot_timeout > 1)
		boot_timeout--;

	/* trigger app-boot */
	else if (boot_timeout == 1)
		cmd = CMD_BOOT_APPLICATION;		
	
}


/*
 * For newer devices (mega88) the watchdog timer remains active even after a
 * system reset. So disable it as soon as possible.
 * automagically called on startup
 */
#if defined (__AVR_ATmega88__) || defined (__AVR_ATmega168__) || defined (__AVR_ATmega328P__)
void disable_wdt_timer(void) __attribute__((naked, section(".init3")));
void disable_wdt_timer(void)
{
	MCUSR = 0;
	WDTCSR = (1<<WDCE) | (1<<WDE);
	WDTCSR = (0<<WDE);
}
#endif

//int main(void) __attribute__ ((noreturn));
int main(void)
{
	
	/* Interrupt Vektoren verbiegen */
	/* tempor�re Variable */
	uint8_t         temp;
	temp = MCUCR;
	MCUCR = temp | (1<<IVCE);
	MCUCR = temp | (1<<IVSEL);
	
	LED_INIT();
	LED_OFF();
	LED_GN_ON();
	
	/* Intel-HEX Zieladresse */
	uint32_t        hex_addr = 0,
	/* Intel-HEX Zieladress-Offset */
	hex_addr_offset = 0,
	/* Zu schreibende Flash-Page */
	flash_page = 0;
	/* Empfangenes Zeichen + Statuscode */
	uint16_t        c = 0,
	/* Intel-HEX Checksumme zum �berpr�fen des Daten */
	hex_check = 0,
	/* Positions zum Schreiben in der Datenpuffer */
	flash_cnt = 0;
	
	/* Flag zum steuern des Programmiermodus */
	uint8_t boot_state = BOOT_STATE_PARSER,
	/* Empfangszustandssteuerung */
	parser_state = PARSER_STATE_START,
	/* Flag zum ermitteln einer neuen Flash-Page */
	flash_page_flag = 1,
	/* Datenpuffer f�r die Hexdaten*/
	flash_data[SPM_PAGESIZE],
	/* Position zum Schreiben in den HEX-Puffer */
	hex_cnt = 0,
	/* Puffer f�r die Umwandlung der ASCII in Bin�rdaten */
	hex_buffer[6],
	/* Intel-HEX Datenl�nge */
	hex_size = 0,
	/* Z�hler f�r die empfangenen HEX-Daten einer Zeile */
	hex_data_cnt = 0,
	/* Intel-HEX Recordtype */
	hex_type = 0,
	/* empfangene HEX-Checksumme */
	hex_checksum=0;
	/* Funktionspointer auf 0x0000 */
	void            (*start)( void ) = 0x0000;
	
	/* F�llen der Puffer mit definierten Werten */
	memset(hex_buffer, 0x00, sizeof(hex_buffer));
	memset(flash_data, 0xFF, sizeof(flash_data));
	
	#ifdef debug
		//DEBUG
		// Init UART
		// Set BAUD
		UBRR0H = UBRRH_VALUE;
		UBRR0L = UBRRL_VALUE;
		#if USE_2X
		UCSR0A |= (1 << U2X0);
		#else
		UCSR0A &= ~(1 << U2X0);
		#endif
	
		// Set frame format to 8 data bits, no parity, 1 stop bit
		UCSR0C |= (1<<UCSZ01)|(1<<UCSZ00);
		//enable reception and RC complete interrupt
		UCSR0B |= (1<<TXEN0)|(1<<RXEN0)|(1<<RXCIE0);
	#endif
	
	//Timer1 Setzen Prescalar 1024 - Mode 0 OVF - 
	TCCR1B = (1<<CS12) | (1<<CS10);
	TCCR1A &= ~((1<<WGM10)|(1<<WGM11));
	TCNT1 = TIMER_RELOAD;
	TIMSK1 |= (1<<TOIE1);


	/* TWI init: set address, auto ACKs with interrupts */
	DDRC &= ~(1<<PORTC0);
	DDRC &= ~(1<<PORTC1);
	DDRC &= ~(1<<PORTC2);
	DDRC &= ~(1<<PORTC3);
	PORTC|= (1<<PC0);
	PORTC|= (1<<PC1);
	PORTC|= (1<<PC2);
	PORTC|= (1<<PC3);
	
	//#define Read_Addr		(0x50 || (PINC & 15))
	
	ADR_INIT;	
	Adr_PullUp;
	TWAR=((0x50|(PINC & 0x0F))<<1);
	//if ((0x50|(PINC & 0x0F))==0x50) LED_RT_ON();

	//TWAR= (0x01<<1); //Adresse setzen
	TWCR &= ~(1<<TWSTA)|(1<<TWSTO);
	TWCR|= (1<<TWEA) | (1<<TWEN)|(1<<TWIE);
	sei();

	recCharF=0;
	GotChar=0;
	addr=0;
	
	do
	{	
		if (GotChar!=0) 
		{
			#ifdef debug	
				sendByte(10);
				sendByte(13);
			#endif
			
			uint8_t twicnt = 0;
			for (twicnt=0;twicnt<GotChar;twicnt++)	
			{
					c=twiBuffer[twicnt];
					twiBuffer[twicnt]=0;
				
				/* Programmzustand: Parser */
				if(boot_state == BOOT_STATE_PARSER)
				{
					switch(parser_state)
					{
						/* Warte auf Zeilen-Startzeichen */
						case PARSER_STATE_START:
						if((uint8_t)c == START_SIGN)
						{
							parser_state = PARSER_STATE_SIZE;
							hex_cnt = 0;
							hex_check = 0;
							//TWCR_ACK;
						}
						break;
						/* Parse Datengr��e */
						case PARSER_STATE_SIZE:
						hex_buffer[hex_cnt++] = (uint8_t)c;
						if(hex_cnt == 2)
						{
							parser_state = PARSER_STATE_ADDRESS;
							hex_cnt = 0;
							hex_size = (uint8_t)hex2num(hex_buffer,2);
							hex_check += hex_size;
						}
						//TWCR_ACK;
						break;
						/* Parse Zieladresse */
						case PARSER_STATE_ADDRESS:
					
						hex_buffer[hex_cnt++] = (uint8_t)c;
						if(hex_cnt == 4)
						{
							parser_state = PARSER_STATE_TYPE;
							hex_cnt = 0;
							hex_addr = hex_addr_offset;
							hex_addr += hex2num(hex_buffer,4);
							hex_check += (uint8_t) hex_addr;
							hex_check += (uint8_t) (hex_addr >> 8);
						}
						//TWCR_ACK;
						break;
						/* Parse Zeilentyp */
						case PARSER_STATE_TYPE:
					
						hex_buffer[hex_cnt++] = (uint8_t)c;
						if(hex_cnt == 2)
						{
							hex_cnt = 0;
							hex_data_cnt = 0;
							hex_type = (uint8_t)hex2num(hex_buffer,2);
							hex_check += hex_type;
						
							switch(hex_type)
							{
								case 1: parser_state = PARSER_STATE_CHECKSUM; break;
								case 0:
								case 2:
								case 4: parser_state = PARSER_STATE_DATA;

								/* Berechnen der neue Flash-Page (abh�ngig von hex_type) */
								/* Liegen die Daten noch in der aktuellen Flash-Page? */
								if(!flash_page_flag && (flash_page != (hex_addr - hex_addr % SPM_PAGESIZE)) )
								{
								
									/* Wenn die Daten nicht in der aktuellen Flash-Page liegen, */
									/* wird die aktuelle Page geschrieben und ein Flag          */
									/* zum berechnen der neuen Page-Startadresse gesetzt        */
									program_page(flash_page, flash_data);
									flash_cnt = 0;
									flash_page_flag = 1;
								}
							
								/* Muss die Page-Startadresse neu berechnet werden? */
								if(flash_page_flag)
								{
									/* Berechnen der neuen Page-Startadresse */
									flash_page = hex_addr - hex_addr % SPM_PAGESIZE;
								
									/* F�llen des Flash-Puffers mit dem "alten" Inhalt der Page */
									memcpy_PF(flash_data, flash_page, SPM_PAGESIZE);
								
									/* Flag setzen um anzuzeigen das eine neue Adresse da ist */
									flash_page_flag = 0;
								}
								break;
								default: parser_state = PARSER_STATE_DATA; break;
							}
						}
						//TWCR_ACK;
						break;
						/* Parse Flash-Daten */
						case PARSER_STATE_DATA:
					
						hex_buffer[hex_cnt++] = (uint8_t)c;
						switch(hex_type)
						{
							case 0:  /* Record Typ 00 - Data Record auswerten */
						
							if(hex_cnt == 2)
							{
								LED_GN_TOGGLE();
							
								hex_cnt = 0;
								flash_data[flash_cnt] = (uint8_t)hex2num(hex_buffer,2);
								hex_check += flash_data[flash_cnt];
								flash_cnt++;
								hex_data_cnt++;
								if(hex_data_cnt == hex_size)
								{
									parser_state = PARSER_STATE_CHECKSUM;
									hex_data_cnt=0;
									hex_cnt = 0;
								}
								/* Puffer voll -> schreibe Page */
								if(flash_cnt == SPM_PAGESIZE)
								{	
									program_page(flash_page, flash_data);
									flash_cnt = 0;
									flash_page_flag = 1;
								}
							}
							break;
							case 2:   /* Record Typ 02 - Extended Segment Address auswerten */
							case 4:   /* Record Typ 04 - Extended Linear Address auswerten */
							if(hex_cnt == 4)
							{
								LED_GN_TOGGLE();
								hex_cnt = 0;
							
								/* Schreibe angfangene Flash-Page vor Segment-Sprung */
								program_page(flash_page, flash_data);
								flash_cnt = 0;
								flash_page_flag = 1;
							
								/* Berechnen der Offsetadresse */
								switch(hex_type)
								{
									case 2: hex_addr_offset = (uint32_t)(hex2num(hex_buffer,4)) << 4; break;
									case 4: hex_addr_offset = (uint32_t)(hex2num(hex_buffer,4)) << 16; break;
								}
							
								/* Addieren der empfangenen Werte f�r die Checksumme */
								hex_check += (uint8_t) hex2num(hex_buffer,2);
								hex_check += (uint8_t) hex2num(hex_buffer,2);
							
								parser_state = PARSER_STATE_CHECKSUM;
								hex_data_cnt=0;
								hex_cnt = 0;
							}
							break;
							
							default:
							if(hex_cnt == 2)
							{
								hex_cnt = 0;
								hex_check += (uint8_t)hex2num(hex_buffer,2);
								hex_data_cnt++;
								if(hex_data_cnt == hex_size)
								{
									parser_state = PARSER_STATE_CHECKSUM;
									hex_data_cnt=0;
									hex_cnt = 0;
								}
								
							}
							break;
						}
						//TWCR_ACK;
						break;
						/* Parse Checksumme */
						case PARSER_STATE_CHECKSUM:
						
						hex_buffer[hex_cnt++] = (uint8_t)c;
						if(hex_cnt == 2)
						{
							hex_checksum = (uint8_t)hex2num(hex_buffer,2);
							hex_check += hex_checksum;
							hex_check &= 0x00FF;
							/* Dateiende -> schreibe Restdaten */
							if(hex_type == 1)
							{
								program_page(flash_page, flash_data);
								boot_state = BOOT_STATE_EXIT;
							}
							/* �berpr�fe Checksumme -> muss '0' sein */
							if(hex_check == 0) 
							{
								parser_state = PARSER_STATE_START;
								//TWCR_ACK;	
							}
							
							else
							{
								
								#ifdef debug
									LED_RT_ON();
									char bt[4];
									itoa(hex_checksum,bt,10);
									sendByte(10);
									sendByte(13);
									sendStr(bt);
									sendByte(10);
									sendByte(13);
									sendStr1(hex_buffer);
								#endif // debug
								parser_state = PARSER_STATE_ERROR;
								//TWCR_NACK;
							}
						}
						break;
						/* Parserfehler (falsche Checksumme) */
						case PARSER_STATE_ERROR:
							
							LED_GN_ON();
							LED_RT_ON();
							TIMSK0 = 0x00;
							TCCR0B = 0x00;
							while (1){};
						break;
						default:
						break;
					}
				}
			}
			GotChar=0;
			sei();
		}		
	}
	while((boot_state!=BOOT_STATE_EXIT) & (cmd!=CMD_BOOT_APPLICATION));

	cli();

	/* Disable TWI but keep address! */
	TWCR = 0x00;

	TIMSK1 = 0x00;
	TCCR1B = 0x00;
	TCNT1=0x00;
	/* Interrupt Vektoren wieder gerade biegen */
	temp = MCUCR;
	MCUCR = temp | (1<<IVCE);
	MCUCR = temp & ~(1<<IVSEL);

	LED_OFF();
	//while (1);
	/* Reset */
//	_delay_ms(1000);
	start();
	return 0;
}
