/* 
* TWI_Display.c
	
	Class for 96x16 Display with SSD1306 and Fonts,Bitmaps and strings stored in F-Ram.
	
	�2015 Walter Sax
	
	YOU CAN USE THIS CODE FOR YOUR PRIVATE USE OR ANY OTHER USE
	AS LONG AS THIS SOFTWARE IF FRRE OF CHARGE AND THE PRODUCTS INCLUDING THIS SOFTWARE ARE ALSO FREE OF CHARGE AND THIS DISCLAIMER IS INCLUDED .
	ANY COMMERCIAL USE OF THIS CODE INCLUDED IN DEVICES OR COMMERCIAL DISTRIBUTION WITHOUT MY PERMISSION IS STRICTLY FORBITTEN.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
	ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
	LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
	SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
	INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
	ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
	POSSIBILITY OF SUCH DAMAGE. 


For the use of Code parts from ADAFRUIT GFX LIBRARY:

This is the core graphics library for all our displays, providing a common
set of graphics primitives (points, lines, circles, etc.).  It needs to be
paired with a hardware-specific library for each display device we carry
(to handle the lower-level functions).

Adafruit invests time and resources providing this open source code, please
support Adafruit & open-source hardware by purchasing products from Adafruit!
 
Copyright (c) 2013 Adafruit Industries.  All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

- Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.
- Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.


For the use of the Arduino SoftI2C library:
 
 * Arduino SoftI2C library. 
 *
 * This is a very fast and very light-weight software I2C-master library 
 * written in assembler. It is based on Peter Fleury's I2C software
 * library: http://homepage.hispeed.ch/peterfleury/avr-software.html
 *
 *
 * This Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the Arduino I2cMaster Library.  If not, see
 * <http://www.gnu.org/licenses/>.


*/

#define SDA_PORT PORTD
#define SDA_PIN PIND3
#define SCL_PORT PORTD
#define SCL_PIN PIND4
//#define I2C_FASTMODE 1
//#define I2C_SLOWMODE 1
// Normal Mode defined in SoftI2CMaster.h for 360kHZ  - if faster PCS9543 is not happy

#define I2C_TIMEOUT 1 // timeout after 10 msec
//#define I1C_NOINTERRUPT 1 // no interrupts
// #define I2C_CPUFREQ (F_CPU/8) // slow down CPU frequency
#include "SoftI2CMaster.h"

//#define EEPROMADDR 0xA6 // set by jumper
#define MAXADDR 0x1FFF

#define Led_Ge_On		PORTB |= (1<<PORTB2)
#define Led_Ge_Off		PORTB &= ~(1<<PORTB2)
#define Led_Ge_Tgl		PORTB ^= (1<<PORTB2)

#define Led_Rt_On		PORTB |= (1<<PORTB1)
#define Led_Rt_Off		PORTB &= ~(1<<PORTB1)
#define Led_Rt_Tgl		PORTB ^= (1<<PORTB1)


#define i2cResINIT		DDRD |= (1<<PORTD5)
#define i2cResHIGH		PORTD |= (1<<PORTD5)
#define i2cResLOW		PORTD &= ~(1<<PORTD5)
//#define dryRun

#include "TWI_Display.h"
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <util/delay.h>
#include <avr/eeprom.h>
#include "RocDisplay_Fonts.h"
// default constructor
void TWI_Display()
{
	//Udeb.init();
	
	i2cResINIT;
	ResetI2CMux();
	lastHour=14;
	lastMinute=0;
	SetupCon=0;
	
	// Timer f�r Blinken
	
	//Timer1 Setzen
	
	TCCR1A &= ~((1<<WGM10)|(1<<WGM11));   // Normal Mode
	TCNT1 =0xE17B;
	TIMSK1 |= (1<<TOIE1);	
	  
	
	//uint8_t tmpbuff[192] = {
	//0x00, 0xFE, 0xFE, 0x86, 0x86, 0x86, 0x86, 0x86, 0x86, 0x86, 0xCE, 0xFC, 0x78, 0x00, 0x00, 0x80,
	//0x40, 0x20, 0x00, 0x00, 0xFE, 0x82, 0x82, 0x82, 0x7C, 0x00, 0xFA, 0x00, 0x90, 0xA8, 0xA8, 0x48,
	//0x00, 0xF0, 0x48, 0x48, 0x30, 0x00, 0x7E, 0x80, 0x00, 0x70, 0x88, 0x88, 0xF8, 0x00, 0x18, 0x20,
	//0xC0, 0x38, 0x00, 0x00, 0x00, 0x00, 0x88, 0x50, 0x20, 0x50, 0x88, 0x00, 0x18, 0x20, 0xF8, 0x20,
	//0x80, 0x00, 0x70, 0x88, 0x70, 0x00, 0x00, 0x00, 0xF0, 0xFC, 0xFC, 0xFE, 0xEF, 0xD7, 0xEF, 0xFF,
	//0xFF, 0xFF, 0xEF, 0xD7, 0xEF, 0xFE, 0xFC, 0xFC, 0xF1, 0x06, 0x38, 0xC6, 0x38, 0xC0, 0x38, 0xC0,
//
	//0x00, 0x7F, 0x7F, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x07, 0x1F, 0x78, 0x60, 0x00, 0x03, 0x04,
	//0x08, 0x10, 0x00, 0x00, 0xF8, 0x28, 0x68, 0x90, 0x00, 0xF8, 0x10, 0x20, 0xF8, 0x00, 0x20, 0x20,
	//0x20, 0x03, 0xF8, 0x28, 0x08, 0x00, 0xF8, 0x00, 0xF8, 0x28, 0x68, 0x90, 0x00, 0xF8, 0x12, 0x21,
	//0x10, 0xF8, 0x00, 0x78, 0x80, 0x60, 0x80, 0x78, 0x00, 0xF0, 0x28, 0xF0, 0x00, 0xF8, 0x28, 0x68,
	//0x90, 0x00, 0xF8, 0xA8, 0x88, 0x00, 0x00, 0x00, 0x07, 0x1F, 0x1F, 0x3D, 0x7B, 0x7B, 0xF7, 0xE7,
	//0xE7, 0xE7, 0xF7, 0x7B, 0x7B, 0x3D, 0x1F, 0x1F, 0x87, 0x60, 0x1C, 0x63, 0x1C, 0x03, 0x1C, 0x03
	//};
	//memcpy(DisSetting[0].buffer,tmpbuff,192);
	//memcpy(DisSetting[1].buffer,tmpbuff,192);
	//memcpy(DisSetting[2].buffer,tmpbuff,192);
	//memcpy(DisSetting[3].buffer,tmpbuff,192);
	
} //TWI_Display

bool StartSoftI2C()
{
	i2c_init();

	return true;
}

void ResetI2CMux()
{
		i2cResLOW;
		_delay_us(10);
		i2cResHIGH;
}

void GetSettings()
{
		SetupCon=0;
		DispWidth=Fram_ReadByte(EE_DispWidth);
		DispHeight=Fram_ReadByte(EE_DispHeight);
		ConDisplays = Fram_ReadByte(EE_DispCnt);

	
	for (uint8_t i=0;i<4;i++)
	{
		
		DisSetting[i].ClockSide = Fram_ReadByte(((i*EE_CntDisSetting)+EE_OffsetDis+EE_DispClkSide));
		DisSetting[i].ShowClock = Fram_ReadByte(((i*EE_CntDisSetting)+EE_OffsetDis+EE_DispClkShow));
		DisSetting[i].DispInvers = Fram_ReadByte(((i*EE_CntDisSetting)+EE_OffsetDis+EE_DispInvers));
		DisSetting[i].DispContrast = Fram_ReadByte(((i*EE_CntDisSetting)+EE_OffsetDis+EE_DispContrast));
		DisSetting[i].DispSync = Fram_ReadByte(((i*EE_CntDisSetting)+EE_OffsetDis+EE_DispSync));
		DisSetting[i].Rotate = Fram_ReadByte(((i*EE_CntDisSetting)+EE_OffsetDis+EE_DispRotate));
		DisSetting[i].BlinkTime = Fram_ReadByte(((i*EE_CntDisSetting)+EE_OffsetDis+EE_Blinktime));
		DisSetting[i].ShowDepCol = Fram_ReadByte(((i*EE_CntDisSetting)+EE_OffsetDis+EE_DispDepCol));
		DisSetting[i].DepColWidth = Fram_ReadByte(((i*EE_CntDisSetting)+EE_OffsetDis+EE_DispDepColWidth));
		if (((ConDisplays>>i)&0x01)==1) DisSetting[i].Active=1; else DisSetting[i].Active=0;
		DisSetting[i].Font[0] = Fram_ReadByte(((i*EE_CntDisSetting)+EE_OffsetDis+EE_DispFont1));
		DisSetting[i].Font[1] = Fram_ReadByte(((i*EE_CntDisSetting)+EE_OffsetDis+EE_DispFont2));
		DisSetting[i].activeLine=0;
		DisSetting[i].activeSection=0;
		DisSetting[i].ClockX=0;
		DisSetting[i].textcolor=WHITE;
		DisSetting[i].TxtMax=DispWidth;
		DisSetting[i].TxtStart=0;
		DisSetting[i].TxtTmimesStart=0;
		memset(DisSetting[i].buffer,0,192);
		doRotation(DisSetting[i].Rotate,i);
		BlinkCnt[i]=DisSetting[i].BlinkTime;
		SetOffset(i);
		
	}
	BlinkTimerStart();
}

bool SetFactoryReset(uint8_t sp)
{
	if ((sp&0x01)==0x01)
	{
		
		Led_Rt_Tgl;
		Fram_WriteByte(EE_DispWidth,96);
		Led_Rt_Tgl;
		Fram_WriteByte(EE_DispHeight,16);
		Led_Rt_Tgl;
		Fram_WriteByte(EE_DispCnt,1);
		Led_Rt_Tgl;

		
		for (uint8_t i=0;i<4;i++)
		{
			Led_Rt_Tgl;
			Fram_WriteByte(((i*EE_CntDisSetting)+EE_OffsetDis+EE_DispClkSide),'L');
			Fram_WriteByte(((i*EE_CntDisSetting)+EE_OffsetDis+EE_DispClkShow),1);
			Fram_WriteByte(((i*EE_CntDisSetting)+EE_OffsetDis+EE_DispInvers),0);
			Fram_WriteByte(((i*EE_CntDisSetting)+EE_OffsetDis+EE_DispContrast),80);
			Fram_WriteByte(((i*EE_CntDisSetting)+EE_OffsetDis+EE_DispSync),0);
			Fram_WriteByte(((i*EE_CntDisSetting)+EE_OffsetDis+EE_DispRotate),2);
			Fram_WriteByte(((i*EE_CntDisSetting)+EE_OffsetDis+EE_Blinktime),2);
			Fram_WriteByte(((i*EE_CntDisSetting)+EE_OffsetDis+EE_DispDepCol),0);
			Fram_WriteByte(((i*EE_CntDisSetting)+EE_OffsetDis+EE_DispDepColWidth),0);
			
			Fram_WriteByte(((i*EE_CntDisSetting)+EE_OffsetDis+EE_DispFont1),0);
			Fram_WriteByte(((i*EE_CntDisSetting)+EE_OffsetDis+EE_DispFont2),0);
		}
	}
	if ((sp&0x02)==0x02)
	{
		uint16_t Address = 0;
		uint16_t tmp = 0;
		Led_Rt_Tgl;
		if (!i2c_start((Fram_Address<<1) | I2C_WRITE)) return false;
		if (!i2c_write((Address>>8)&0xFF)) return false;
		if (!i2c_write(Address&0xFF)) return false;
		for (uint16_t i = 0;i<420;i++)
		{
			if (!i2c_write(pgm_read_byte(&Bitmap_20x8[tmp]))) return false;	
			Address++;
			tmp++;
			Led_Rt_Tgl;
		}
		i2c_stop();
		
		tmp=0;
		if (!i2c_start((Fram_Address<<1) | I2C_WRITE)) return false;
		if (!i2c_write((Address>>8)&0xFF)) return false;
		if (!i2c_write(Address&0xFF)) return false;
		for (uint16_t i=0;i<1568;i++)
		{
			if (!i2c_write(pgm_read_byte(&FONT_7x5[tmp]))) return false;
			Address++;
			tmp++;
			Led_Rt_Tgl;
		}
		i2c_stop();
		
		tmp=0;
		if (!i2c_start((Fram_Address<<1) | I2C_WRITE)) return false;
		if (!i2c_write((Address>>8)&0xFF)) return false;
		if (!i2c_write(Address&0xFF)) return false;
		for (uint16_t i=0;i<1568;i++)
		{
			if (!i2c_write(pgm_read_byte(&FONT_6x5w[tmp]))) return false;
			Address++;
			tmp++;
			Led_Rt_Tgl;
		}
		i2c_stop();
		
		tmp=0;
		if (!i2c_start((Fram_Address<<1) | I2C_WRITE)) return false;
		if (!i2c_write((Address>>8)&0xFF)) return false;
		if (!i2c_write(Address&0xFF)) return false;
		for (uint16_t i=0;i<1568;i++)
		{
			if (!i2c_write(pgm_read_byte(&FONT_6x5n[tmp]))) return false;
			Address++;
			tmp++;
			Led_Rt_Tgl;
		}
		i2c_stop();
		
		tmp=0;
		if (!i2c_start((Fram_Address<<1) | I2C_WRITE)) return false;
		if (!i2c_write((Address>>8)&0xFF)) return false;
		if (!i2c_write(Address&0xFF)) return false;
		for (uint16_t i=0;i<1568;i++)
		{
			if (!i2c_write(pgm_read_byte(&FONT_5x5iz[tmp]))) return false;
			Address++;
			tmp++;
			Led_Rt_Tgl;
		}
		i2c_stop();		
	}
	if ((sp&0x04)==0x04)
	{
		uint16_t Address = 6692;
		if (!i2c_start((Fram_Address<<1) | I2C_WRITE)) return false;
		if (!i2c_write((Address>>8)&0xFF)) return false;
		if (!i2c_write(Address&0xFF)) return false;
		for (uint16_t i=6692;i<7312;i++)
		{
			if (!i2c_write(0x00)) return false;
			Address++;
			Led_Rt_Tgl;
		}
		i2c_stop();
	}
	Led_Rt_Off;
	Fram_WriteByte(EE_FirstStart,128);
	return true;
}

void SetShowDepartureCol(uint8_t Show,uint8_t Display)
{
	DisSetting[Display].ShowDepCol=Show;
	Fram_WriteByte(((Display*EE_CntDisSetting)+EE_OffsetDis+EE_DispDepCol),Show);
	
	SetOffset(Display);	
}

void SetDepartureCol(uint8_t ColWidth, uint8_t Display)
{
	DisSetting[Display].DepColWidth=ColWidth;
	Fram_WriteByte(((Display*EE_CntDisSetting)+EE_OffsetDis+EE_DispDepColWidth),ColWidth);
	
	SetOffset(Display);
}

void SetDisplaySize(uint8_t Width, uint8_t Height)
{
	DispWidth=Width;
	DispHeight=Height;
	Fram_WriteByte(EE_DispHeight,Height);
	
	Fram_WriteByte(EE_DispWidth,Width);
	
}

uint8_t GetDisplayWidth()
{
	return DispWidth;
}

uint8_t GetDisplayHeight()
{
	return DispHeight;
}

void SetDisplayCount(uint8_t Count)
{
	uint8_t oldCon = ConDisplays;
	ConDisplays = Count;
	Fram_WriteByte(EE_DispCnt,Count);
	if ( ((ConDisplays>>0)&0x01)!=((oldCon>>0)&0x01)  )
	{
		if (((ConDisplays>>0)&0x01)==1)
		{	
			DisSetting[0].Active=1;
			StartSingleDisplay(0);
		}
		else 
		{
			TurnOffDisplay(0);
			DisSetting[0].Active=0;
		}	
	}
	
	if ( ((ConDisplays>>1)&0x01)!=((oldCon>>1)&0x01)  )
	{
		if (((ConDisplays>>1)&0x01)==1)
		{
			DisSetting[1].Active=1;
			StartSingleDisplay(1);
		}
		else
		{
			TurnOffDisplay(1);
			DisSetting[1].Active=0;
		}
	}
	
	if ( ((ConDisplays>>2)&0x01)!=((oldCon>>2)&0x01)  )
	{
		if (((ConDisplays>>2)&0x01)==1)
		{
			DisSetting[2].Active=1;
			StartSingleDisplay(2);
		}
		else
		{
			TurnOffDisplay(2);
			DisSetting[2].Active=0;
		}
	}
	
	if ( ((ConDisplays>>3)&0x01)!=((oldCon>>3)&0x01)  )
	{
		if (((ConDisplays>>3)&0x01)==1)
		{
			DisSetting[3].Active=1;
			StartSingleDisplay(3);
		}
		else
		{
			TurnOffDisplay(3);
			DisSetting[3].Active=0;
		}
	}
	
	//if (((ConDisplays>>1)&0x01)==1) DisSetting[1].Active=1; else DisSetting[1].Active=0;
	//if (((ConDisplays>>2)&0x01)==1) DisSetting[2].Active=1; else DisSetting[2].Active=0;
	//if (((ConDisplays>>3)&0x01)==1) DisSetting[3].Active=1; else DisSetting[3].Active=0;
}

void SetSyncDisplays(uint8_t DispSync)
{
	DisSetting[ActiveDisplay].DispSync = DispSync;
	Fram_WriteByte(((ActiveDisplay*EE_CntDisSetting)+EE_OffsetDis+EE_DispSync),DispSync);
	
}

void SetDisplayClock(uint8_t ClkDisp,uint8_t Display )
{
	DisSetting[Display].ShowClock = ClkDisp;
	Fram_WriteByte(((Display*EE_CntDisSetting)+EE_OffsetDis+EE_DispClkShow),ClkDisp);
	
	SetOffset(Display);
}

void SetClockSide(char Side,uint8_t Display) //False=Left true=Right
{
	DisSetting[Display].ClockSide=(uint8_t)Side;
	Fram_WriteByte(((Display*EE_CntDisSetting)+EE_OffsetDis+EE_DispClkSide),(uint8_t)Side);
	
	SetOffset(Display);
}

void SetDisplayInvert(uint8_t Invert,uint8_t Display)
{
	uint8_t tmpAct = ActiveDisplay;
	SetActiveDisplay(Display,true);
	DisSetting[Display].DispInvers=Invert;
	//if ((DisSetting[Display].Active&0x01)==1) Dis_invertDisplay((Invert&0x01));
	Dis_invertDisplay((Invert&0x01));
	Fram_WriteByte(((Display*EE_CntDisSetting)+EE_OffsetDis+EE_DispInvers),Invert);
	
	SetActiveDisplay(tmpAct,true);
}

void SetKontrast(uint8_t Kontrast,uint8_t Display)
{
	uint8_t tmpAct = ActiveDisplay;
	SetActiveDisplay(Display,true);
	DisSetting[Display].DispContrast=Kontrast;
	Dis_dim(Kontrast);
	Fram_WriteByte(((Display*EE_CntDisSetting)+EE_OffsetDis+EE_DispContrast),Kontrast);
	SetActiveDisplay(tmpAct,true);	
}

void  SetFont(uint8_t font)
{
	uint8_t tmpFont = DisSetting[ActiveDisplay].Font[DisSetting[ActiveDisplay].activeLine] & (15<<((~(DisSetting[ActiveDisplay].activeSection)&0x01)*4));
	DisSetting[ActiveDisplay].Font[DisSetting[ActiveDisplay].activeLine]=((font&0x0f)<<(4*DisSetting[ActiveDisplay].activeSection))|tmpFont;
	
	// Das ist das selbe wie der auskommentierte Block - nur 70 Bytes kleiner im Flash !!!!!
	Fram_WriteByte(((ActiveDisplay*EE_CntDisSetting)+EE_OffsetDis+(EE_DispFont1+DisSetting[ActiveDisplay].activeLine)),DisSetting[ActiveDisplay].Font[DisSetting[ActiveDisplay].activeLine]);
	
	//if (DisSetting[ActiveDisplay].activeLine==0)
	//Fram_WriteByte(((ActiveDisplay*EE_CntDisSetting)+EE_OffsetDis+EE_DispFont1),DisSetting[ActiveDisplay].Font[DisSetting[ActiveDisplay].activeLine]);
	//if (DisSetting[ActiveDisplay].activeLine==1)
	//Fram_WriteByte(((ActiveDisplay*EE_CntDisSetting)+EE_OffsetDis+EE_DispFont2),DisSetting[ActiveDisplay].Font[DisSetting[ActiveDisplay].activeLine]);
	//
}

uint8_t GetFont()
{
	return ((DisSetting[ActiveDisplay].Font[DisSetting[ActiveDisplay].activeLine]>>(4*DisSetting[ActiveDisplay].activeSection))&0x07);
}

void SetOffset(uint8_t Display)
{
	if (DisSetting[Display].ShowClock==1) // DisplayClock
	{
		if ((char)DisSetting[Display].ClockSide=='L') // Left
		{
			DisSetting[Display].ClockX = 0;
			DisSetting[Display].TxtStart = ClockWidth + 2;
			if (DisSetting[Display].ShowDepCol==1)  // Show DepCol
			{
				DisSetting[Display].TxtTimesMax = DispWidth;
				DisSetting[Display].TxtTmimesStart = DispWidth-DisSetting[Display].DepColWidth;
				DisSetting[Display].TxtMax = DispWidth-DisSetting[Display].DepColWidth-1;
			}
			else
			{
				DisSetting[Display].TxtMax = DispWidth;
				DisSetting[Display].TxtTmimesStart=0;
				DisSetting[Display].TxtTimesMax=0;
			}
		}
		else
		{
			DisSetting[Display].ClockX = DispWidth-ClockWidth;
			DisSetting[Display].TxtStart = 0;
			if (DisSetting[Display].ShowDepCol==1)  // Show DepCol
			{
				DisSetting[Display].TxtTimesMax = DisSetting[Display].ClockX-2;
				DisSetting[Display].TxtTmimesStart = DisSetting[Display].TxtTimesMax-DisSetting[Display].DepColWidth;
				DisSetting[Display].TxtMax = DisSetting[Display].TxtTmimesStart-1;
			}
			else
			{
				DisSetting[Display].TxtMax = DisSetting[Display].ClockX-1;
				DisSetting[Display].TxtTmimesStart=0;
				DisSetting[Display].TxtTimesMax=0;
			}
		}
	}
	else
	{
		DisSetting[Display].TxtStart = 0;
		if (DisSetting[Display].ShowDepCol==1)	//Show DepCol
		{
			DisSetting[Display].TxtTimesMax = DispWidth;
			DisSetting[Display].TxtTmimesStart = DispWidth-DisSetting[Display].DepColWidth;
			DisSetting[Display].TxtMax = DispWidth-DisSetting[Display].DepColWidth-1;
		}
		else
		{
			DisSetting[Display].TxtMax = DispWidth;	
			DisSetting[Display].TxtTmimesStart=0;
			DisSetting[Display].TxtTimesMax=0;
		}
	}
	DisSetting[Display].activeLine=0;
	DisSetting[Display].activeSection=0;
	cursor_y[Display]=0;
	cursor_x[Display][0]=DisSetting[Display].TxtStart;
	cursor_x[Display][1]=DisSetting[Display].TxtStart;
}

void SetClock(uint8_t hour,uint8_t minute, bool redraw)
{
	uint16_t ColBox = WHITE; 
	uint16_t ColCir = BLACK;
	uint8_t tmpActive = ActiveDisplay;
	for (uint8_t i=0;i<4;i++)
	{
		if ((DisSetting[i].Active==1) & (DisSetting[i].ShowClock==1))
		{	
			SetActiveDisplay(i,true);
			ColBox =(0x01&(DisSetting[i].DispInvers>>7));
			ColCir = (0x01&(~ColBox));

			drawBox(DisSetting[i].ClockX-1,0,DisSetting[i].ClockX+ClockWidth+1,16,ColBox);			
			fillCircle((uint16_t)(DisSetting[i].ClockX+7),(uint16_t)(7),7,ColCir);
			if (DisSetting[i].ClockSide=='L')
			{
				drawLine((uint16_t)(DisSetting[i].ClockX+7),(uint16_t)(7),(uint16_t)(DisSetting[i].ClockX+7+GetXYFromTime(minute,hour,ReturnHour,ReturnX)), 7-GetXYFromTime(minute,hour,ReturnHour,ReturnY),ColBox );
				drawLine((uint16_t)(DisSetting[i].ClockX+7),(uint16_t)(7),(uint16_t)(DisSetting[i].ClockX+7+GetXYFromTime(minute,hour,ReturnMinute,ReturnX)), 7-GetXYFromTime(minute,hour,ReturnMinute,ReturnY),ColBox );
			}
			if (DisSetting[i].ClockSide=='R')
			{
				drawLine((uint16_t)(DisSetting[i].ClockX+7),(uint16_t)(7),(uint16_t)(DisSetting[i].ClockX+7+GetXYFromTime(minute,hour,ReturnHour,ReturnX)), 7-GetXYFromTime(minute,hour,ReturnHour,ReturnY),ColBox );
				drawLine((uint16_t)(DisSetting[i].ClockX+7),(uint16_t)(7),(uint16_t)(DisSetting[i].ClockX+7+GetXYFromTime(minute,hour,ReturnMinute,ReturnX)), 7-GetXYFromTime(minute,hour,ReturnMinute,ReturnY),ColBox );
			}
			if (redraw) Dis_display(true);
		}
	}
	lastHour=hour;
	lastMinute=minute;
	SetActiveDisplay(tmpActive,true);
}

void SetActiveLine(uint8_t Line)
{
	DisSetting[ActiveDisplay].activeLine=Line;
	cursor_y[ActiveDisplay] = Line*8;
	DisSetting[ActiveDisplay].activeLine=Line;
	if (DisSetting[ActiveDisplay].activeSection==0)
	{
		cursor_x[ActiveDisplay][DisSetting[ActiveDisplay].activeLine]=DisSetting[ActiveDisplay].TxtStart;	
	}
	else
	{
		cursor_x[ActiveDisplay][DisSetting[ActiveDisplay].activeLine]=DisSetting[ActiveDisplay].TxtTmimesStart;
	}	
}

void SetActiveSection(uint8_t Section)
{
	DisSetting[ActiveDisplay].activeSection=Section;
	if (Section==0)
	{
		cursor_x[ActiveDisplay][DisSetting[ActiveDisplay].activeLine]=DisSetting[ActiveDisplay].TxtStart;
	}
	else
	{
		cursor_x[ActiveDisplay][DisSetting[ActiveDisplay].activeLine]=DisSetting[ActiveDisplay].TxtTmimesStart;
	}
}

void SetCsr_X(uint8_t x)
{
	cursor_x[ActiveDisplay][DisSetting[ActiveDisplay].activeLine]=x;
}

void SetCsr_Y(uint8_t y)
{
	cursor_y[ActiveDisplay] = y;
}

void SetActiveDisplay(uint8_t Display,bool SetMux)
{
	ActiveDisplay=Display;
	if ( ((SetMux) & (DisSetting[Display].Active==1)) )
	{
		//ResetI2CMux();
	
		if (!i2c_start((Switch_Address<<1)|I2C_WRITE));
		if (!i2c_write((1<<Display)));
		i2c_stop();
		
	}
}

uint8_t isActive()
{
	return (DisSetting[ActiveDisplay].Active);	
}

void BlinkStart ()
{
	DisSetting[ActiveDisplay].xBlinkStart = cursor_x[ActiveDisplay][DisSetting[ActiveDisplay].activeLine];
	DisSetting[ActiveDisplay].BlinkLine = DisSetting[ActiveDisplay].activeLine;
	DisSetting[ActiveDisplay].AltPos=1;
	DisSetting[ActiveDisplay].AltPos1=1;
	//DisSetting[ActiveDisplay].BlinkState=0;
	RamBufferSize=96;
	memset(RamBuffer,0,RamBufferSize);
	Fram_WriteBlinbuffer(0);
	DisSetting[ActiveDisplay].BlinkState |=0x01;
	Fram_WriteBlinbuffer(0);
	DisSetting[ActiveDisplay].BlinkState = 0x02;
	BlinkCnt[ActiveDisplay]=DisSetting[ActiveDisplay].BlinkTime;
}

void BlinkEnd ()
{
	
	if ((DisSetting[ActiveDisplay].BlinkLine) != DisSetting[ActiveDisplay].activeLine) 
	{
		DisSetting[ActiveDisplay].xBlinkEnd=0;
		//GPIOR0 = 0;
		return;
	}
	if ((DisSetting[ActiveDisplay].xBlinkEnd-DisSetting[ActiveDisplay].xBlinkStart)>96) 
	{
		DisSetting[ActiveDisplay].xBlinkEnd=0;
		//GPIOR0 = 0;
		return;
	}
	if (DisSetting[ActiveDisplay].AltPos>=DisSetting[ActiveDisplay].AltPos1)
		cursor_x[ActiveDisplay][DisSetting[ActiveDisplay].activeLine]=DisSetting[ActiveDisplay].xBlinkStart+DisSetting[ActiveDisplay].AltPos;
	
	if (DisSetting[ActiveDisplay].AltPos<DisSetting[ActiveDisplay].AltPos1)
		cursor_x[ActiveDisplay][DisSetting[ActiveDisplay].activeLine]=DisSetting[ActiveDisplay].xBlinkStart+DisSetting[ActiveDisplay].AltPos1;
	
	DisSetting[ActiveDisplay].xBlinkEnd = cursor_x[ActiveDisplay][DisSetting[ActiveDisplay].activeLine];
	DisSetting[ActiveDisplay].BlinkState = 0x80;//(1<<7);
}

void DoBlink()
{
	
	uint8_t x = DisSetting[ActiveDisplay].xBlinkStart;
	//uint8_t x1 = DisSetting[ActiveDisplay].xBlinkEnd;
	uint8_t y = (DisSetting[ActiveDisplay].BlinkLine)*8;
	
	if (DisSetting[ActiveDisplay].xBlinkEnd > 0)
	{
		Led_Ge_Tgl;
		
		Fram_ReadBlinkBuffer();
		
		RamBufferSize=DisSetting[ActiveDisplay].xBlinkEnd-DisSetting[ActiveDisplay].xBlinkStart;
		SetCsr_X(x);
		SetCsr_Y(y);
		
		drawChar(WHITE);
		if ((DisSetting[ActiveDisplay].BlinkState&0x01) == 0) // Bytes im Hauptbuffer auf temp1 und temp auf Hauptbuffer
		{
			DisSetting[ActiveDisplay].BlinkState |= 0x01;
		}
		else // Bytes vom Temp1 auf Haupt Buffer in den Display Buffer rein
		{	
			DisSetting[ActiveDisplay].BlinkState &= ~(0x01);
		}
		Dis_display(false);
		Led_Ge_Tgl;
	}
}

void WriteAlternate(uint8_t c,uint8_t bf)
{
	//if (((c > 0x1f) & (c < 0x7f)) || (c > 0xa0))	// Draw char
	{
		
		Fram_ReadFont(c,GetFont());
		
		//for (uint8_t i = 0; i<RamBufferSize;i++)
		{
			if (bf==0)
			{
				
				DisSetting[ActiveDisplay].BlinkState &= ~(1);
				//if (DisSetting[ActiveDisplay].AltPos>95) return
				Fram_WriteBlinbuffer(DisSetting[ActiveDisplay].AltPos);
				DisSetting[ActiveDisplay].AltPos+=RamBufferSize;
			}
			if (bf==1)
			{
				DisSetting[ActiveDisplay].BlinkState |= (1);
				//if (DisSetting[ActiveDisplay].AltPos1>95) return
				Fram_WriteBlinbuffer(DisSetting[ActiveDisplay].AltPos1);
				DisSetting[ActiveDisplay].AltPos1+=RamBufferSize;
			}
		}
	}
}

void AlternateBMP(uint8_t Nr,uint8_t bf)
{

	Fram_ReadSmallBMP(Nr);
	//for (uint8_t i=0;i<RamBufferSize;i++)
	{
		if (bf==0)
		{
			DisSetting[ActiveDisplay].BlinkState &= ~(1);
			//if (DisSetting[ActiveDisplay].AltPos>95) return
			Fram_WriteBlinbuffer(DisSetting[ActiveDisplay].AltPos);
			DisSetting[ActiveDisplay].AltPos+=RamBufferSize;
		}
		if (bf==1)
		{
			DisSetting[ActiveDisplay].BlinkState |= (1);
			//if (DisSetting[ActiveDisplay].AltPos1>95) return
			Fram_WriteBlinbuffer(DisSetting[ActiveDisplay].AltPos1);
			DisSetting[ActiveDisplay].AltPos1+=RamBufferSize;
		}
	}
}

void BlinkTimerStart()
{
	TCCR1B = (1<<CS12) | (1<<CS10);
}

void BlinkTimerStop()
{
	TCCR1B &= ~((1<<CS12) | (1<<CS11) | (1<<CS10));
}

void SetBlinkTime(uint8_t BlkTime,uint8_t Display)
{
	Fram_WriteByte(((Display*EE_CntDisSetting)+EE_OffsetDis+EE_Blinktime),BlkTime);
	DisSetting[Display].BlinkTime=BlkTime;
}

uint8_t I2CTest()
{
	uint8_t con =0;
	
	for (uint8_t i = 1;i<127;i++)
	{
		
		if (i2c_start((i<<1)|I2C_WRITE))
		{
			i2c_stop();
			con++;
		}
		i2c_init();
	}
	if (con==0)
	{
		Led_Ge_Off;
		Led_Rt_Off;
	}
	else if (con==1)
	{
		Led_Rt_On;
		Led_Ge_Off;
	}
	else if (con==2)
	{
		Led_Rt_On;
		Led_Ge_On;
	}
	ResetI2CMux();
	/*
	if (i2c_start((Fram_Address<<1)|I2C_WRITE)) con++;
	i2c_stop();
	if (i2c_start((Display_Address<<1)|I2C_WRITE)) con++;
	i2c_stop();
	*/
	return con;
	
}

// Write a Byte into FRam
bool Fram_WriteByte(uint16_t Address,uint8_t byte)
{
	if (MAXADDR < Address) return false;
	if (!i2c_start((Fram_Address<<1) | I2C_WRITE)) return false;
	if (!i2c_write((Address>>8)&0xFF)) return false;
	if (!i2c_write(Address&0xFF)) return false;
	if (!i2c_write(byte)) return false;
	i2c_stop();
	return true;
}

uint8_t Fram_ReadByte(uint16_t Address) 
{	
	uint8_t tempResult = 0x00;
	if (MAXADDR < Address) return false;
	if (!i2c_start((Fram_Address<<1) | I2C_WRITE )) return false;
	if (!i2c_write((Address>>8)&0xFF)) return false;
	if (!i2c_write(Address&0xFF)) return false;
	if (!i2c_rep_start((Fram_Address<<1) | I2C_READ)) return false;
	tempResult = i2c_read(true);
	i2c_stop();
	return tempResult;
}

bool Fram_WriteSmallBMP(uint8_t BmpNr)
{
	
	if (19<BmpNr) return false;
	if (!i2c_start((Fram_Address<<1) | I2C_WRITE )) return false;
	if (!i2c_write((AddrSmallBMP>>8)&0xFF)) return false;
	if (!i2c_write(AddrSmallBMP&0xFF)) return false;
	for (uint8_t i = 0; i < 21; i++)
	{
		if (!i2c_write(RamBuffer[i])) return false;
	}
	i2c_stop();
	return true;
}

bool Fram_ReadSmallBMP(uint8_t BmpNr)
{
		
		if (19 < BmpNr ) return false;
		memset(RamBuffer,0x00,sizeof(RamBuffer));;
		if (!i2c_start((Fram_Address<<1) | I2C_WRITE )) return false;
		if (!i2c_write((AddrSmallBMP>>8)&0xFF)) return false;
		if (!i2c_write(AddrSmallBMP&0xFF)) return false;
		if (!i2c_rep_start((Fram_Address<<1) | I2C_READ)) return false;
		RamBufferSize = i2c_read(false);
		if (RamBufferSize ==0) i2c_read(true);
		for (uint8_t i=0;i<RamBufferSize;i++)
		{
			RamBuffer[i]=i2c_read((i==RamBufferSize-1));
		}
		//RamBuffer[30]=i2c_read(true);
		i2c_stop();
		return true;
}

bool Fram_WriteFont(char Chr,uint8_t Font)
{
	//i2c_init();
	if (RamBufferSize>7 ) return false;
	if (!i2c_start((Fram_Address<<1) | I2C_WRITE )) return false;
	if (!i2c_write((AddrChrFont>>8)&0xFF)) return false;
	if (!i2c_write(AddrChrFont&0xFF)) return false;
	for (uint8_t i = 0; i < 7; i++)
	{
		if (!i2c_write(RamBuffer[i])) return false;
	}
	i2c_stop();
	return true;
}

bool Fram_ReadFont(char Chr,uint8_t Font)
{
	//i2c_init();
	memset(RamBuffer,0x00,sizeof(RamBuffer));
	if (!i2c_start((Fram_Address<<1) | I2C_WRITE )) return false;
	if (!i2c_write((AddrChrFont>>8)&0xFF)) return false;
	if (!i2c_write(AddrChrFont&0xFF)) return false;
	if (!i2c_rep_start((Fram_Address<<1) | I2C_READ)) return false;
	RamBufferSize = i2c_read(false);
	if (RamBufferSize==0) i2c_read(true);
	for (uint8_t i=0;i<RamBufferSize;i++)
	{
		RamBuffer[i]=i2c_read((i==RamBufferSize-1));
	}
	i2c_stop();
	return true;
}

bool Fram_SaveTxt(uint8_t *txt,uint8_t TxtNo)
{
	
	if (!i2c_start((Fram_Address<<1) | I2C_WRITE )) return false;
	if (!i2c_write((AddrTxtBlock>>8)&0xFF)) return false;
	if (!i2c_write(AddrTxtBlock&0xFF)) return false;
	for (uint8_t i = 0; i < 31; i++)
	{
		if (!i2c_write(txt[i])) return false;
	}
	i2c_stop();
	return true;
}

bool Fram_LoadTxt(uint8_t TxtNo)
{
	memset(RamBuffer,0x00,sizeof(RamBuffer));
	if (!i2c_start((Fram_Address<<1) | I2C_WRITE )) return false;
	if (!i2c_write((AddrTxtBlock>>8)&0xFF)) return false;
	if (!i2c_write(AddrTxtBlock&0xFF)) return false;
	if (!i2c_rep_start((Fram_Address<<1) | I2C_READ)) return false;
	RamBufferSize = i2c_read(false);
	if (RamBufferSize==0) i2c_read(true);
	for (uint8_t i=0;i<RamBufferSize;i++)
	{
		RamBuffer[i]=i2c_read((i==(RamBufferSize-1)));
	}
	i2c_stop();
	return true;
}

void Fram_WriteBlinbuffer(uint8_t pos)
{
	if (!i2c_start((Fram_Address<<1) | I2C_WRITE )) return ;
	if (!i2c_write(((AddrBlinBuff >>8)&0xFF))) return ;
	if (!i2c_write((AddrBlinBuff &0xFF))) return ;
	if ((pos+RamBufferSize)>96)
	{
		if (!i2c_write(96)) return;
	}
	else
	{
		if (!i2c_write((pos+RamBufferSize))) return;
	}
	i2c_stop();
	
	if (!i2c_start((Fram_Address<<1) | I2C_WRITE )) return ;
		if (!i2c_write((((AddrBlinBuff)+pos) >>8)&0xFF)) return ;
		if (!i2c_write(((AddrBlinBuff)+pos) &0xFF)) return ;
	for (uint8_t i = 0; i < RamBufferSize; i++)
	{
		if ((i+pos)<96)
		{
			if (!i2c_write(RamBuffer[i])) return;
		}
		else
			{
				i2c_stop();
				return;
			}
	}
	i2c_stop();
}

void Fram_ReadBlinkBuffer()
{
	memset(RamBuffer,0x00,sizeof(RamBuffer));
	if (!i2c_start((Fram_Address<<1) | I2C_WRITE )) return;
	if (!i2c_write((AddrBlinBuff>>8)&0xFF)) return;
	if (!i2c_write(AddrBlinBuff&0xFF)) return;
	if (!i2c_rep_start((Fram_Address<<1) | I2C_READ)) return;
	RamBufferSize = i2c_read(false);
	if (RamBufferSize==0) i2c_read(true);
	for (uint8_t i=0;i<RamBufferSize;i++)
	{
		RamBuffer[i]=i2c_read((i==(RamBufferSize-1)));
	}
	i2c_stop();
	return;
}


int8_t GetXYFromTime(double Minute, double Hour, uint8_t WhatReturn, uint8_t WantXY)
{
	double deg = 90;
	double rad = 6;
	if (WhatReturn==ReturnMinute)
	{
		if ((360.0-(Minute*6.0)+90.0)-360.0 >-1.0)
		{
			deg = (360.0-(Minute*6.0)+90.0)-360.0;
		}
		else
		{
			deg = (360.0-(Minute*6.0)+90.0);
		}
		if (WantXY==ReturnX)
			return GetXFromPolar(deg,rad);
		else if (WantXY==ReturnY)
			return GetYFromPolar(deg,rad);
		else 
			return 0;
	}
	else if (WhatReturn==ReturnHour)
	{
		rad = 4;
		if (11<Hour) Hour -=12.0;
		if (((360.0-(Hour*30.0)+90.0)-360.0) - (Minute*(0.5)) >-1.0)
		{
			deg = ((360.0-(Hour*30.0)+90.0)-360.0) - (Minute*(0.5));
		}
		else
		{
			deg = (360.0-(Hour*30.0)+90.0) - (Minute*(0.5));
		}
		if (WantXY==ReturnX)
			return GetXFromPolar(deg,rad);
		else if (WantXY==ReturnY)
			return GetYFromPolar(deg,rad);
		else 
			return 0;
	}
	else
	{
		return 0;
	}
}

int8_t GetXFromPolar(double deg, double rad)
{
	return (uint8_t)(rad * (cos(deg*((M_PI*2.0)/360.0)))); 
}

int8_t GetYFromPolar(double deg, double rad)
{
	return (uint8_t)rad * (sin(deg*((M_PI*2.0)/360.0)));
}


void Dis_data(uint8_t c) 
{
	// I2C
	uint8_t control = 0x40;   // Co = 0, D/C = 1
	i2c_start((Display_Address<<1) | I2C_WRITE);
	i2c_write(control);
	i2c_write(c);
	i2c_stop();
	
}

void Dis_command(uint8_t c) 
{

	// I2C
	uint8_t control = 0x80;   // Co = 0, D/C = 0
	if (!i2c_start((Display_Address<<1) | I2C_WRITE)) Led_Ge_Tgl;
	if (!i2c_write(control)) Led_Ge_Tgl ;
	if (!i2c_write(c)) Led_Ge_Tgl;
	i2c_stop();
	_delay_ms(1);
	//i2c_init();
	
}

void Dis_StartDisplay()
{
	RST_CLR;
	_delay_ms(3);
	RST_SET;
	_delay_ms(10);

	for (uint8_t i = 0;i<4;i++)
	{
		if (DisSetting[i].Active==1)
		{
			StartSingleDisplay(i);
			//SetActiveDisplay(i,true);
			//// Init sequence for 96x16 OLED module
			//i2c_init();
			//Dis_command(SSD1306_DISPLAYOFF);                    // 0xAE
			//Dis_command(SSD1306_SETDISPLAYCLOCKDIV);            // 0xD5
			//Dis_command(0x80);                                  // the suggested ratio 0x80
			//Dis_command(SSD1306_SETMULTIPLEX);                  // 0xA8
			//Dis_command(0x0F);
			//Dis_command(SSD1306_SETDISPLAYOFFSET);              // 0xD3
			//Dis_command(0x00);                                   // no offset
			//Dis_command(SSD1306_SETSTARTLINE | 0x0);            // line #0
			//Dis_command(SSD1306_CHARGEPUMP);                    // 0x8D
			//Dis_command(0x14); 
			//Dis_command(SSD1306_MEMORYMODE);                    // 0x20
			//Dis_command(0x00);                                  // 0x0 act like ks0108
			//Dis_command(SSD1306_SEGREMAP | 0x1);
			//Dis_command(SSD1306_COMSCANDEC);
			//Dis_command(SSD1306_SETCOMPINS);                    // 0xDA
			//Dis_command(0x2);	//ada x12
			//Dis_command(SSD1306_SETCONTRAST);                   // 0x81
			//Dis_command(0xAF); 
			//Dis_command(SSD1306_SETPRECHARGE);                  // 0xd9
			//Dis_command(0xF1); 
			//Dis_command(SSD1306_SETVCOMDETECT);                 // 0xDB
			//Dis_command(0x20);
			//Dis_command(SSD1306_DISPLAYALLON_RESUME);           // 0xA4
			//Dis_command(SSD1306_NORMALDISPLAY);
//
			//Dis_command(SSD1306_DISPLAYON);//--turn on oled panel
	//
			//if ((DisSetting[i].DispInvers&0x01)==1) Dis_command(SSD1306_INVERTDISPLAY);
			//Dis_dim(DisSetting[i].DispContrast);
			//Dis_clearDisplay();
			//Dis_display(false);
		}
	}
	SetActiveDisplay(0,true);
}

void StartSingleDisplay(uint8_t Display)
{
	uint8_t tempDis = ActiveDisplay;
		if (DisSetting[Display].Active==1)
		{
			SetActiveDisplay(Display,true);
			// Init sequence for 96x16 OLED module
			i2c_init();
			Dis_command(SSD1306_DISPLAYOFF);                    // 0xAE
			Dis_command(SSD1306_SETDISPLAYCLOCKDIV);            // 0xD5
			Dis_command(0x80);                                  // the suggested ratio 0x80
			Dis_command(SSD1306_SETMULTIPLEX);                  // 0xA8
			Dis_command(0x0F);
			Dis_command(SSD1306_SETDISPLAYOFFSET);              // 0xD3
			Dis_command(0x00);                                   // no offset
			Dis_command(SSD1306_SETSTARTLINE | 0x0);            // line #0
			Dis_command(SSD1306_CHARGEPUMP);                    // 0x8D
			Dis_command(0x14);
			Dis_command(SSD1306_MEMORYMODE);                    // 0x20
			Dis_command(0x00);                                  // 0x0 act like ks0108
			Dis_command(SSD1306_SEGREMAP | 0x1);
			Dis_command(SSD1306_COMSCANDEC);
			Dis_command(SSD1306_SETCOMPINS);                    // 0xDA
			Dis_command(0x2);	//ada x12
			Dis_command(SSD1306_SETCONTRAST);                   // 0x81
			Dis_command(0xAF);
			Dis_command(SSD1306_SETPRECHARGE);                  // 0xd9
			Dis_command(0xF1);
			Dis_command(SSD1306_SETVCOMDETECT);                 // 0xDB
			Dis_command(0x20);
			Dis_command(SSD1306_DISPLAYALLON_RESUME);           // 0xA4
			Dis_command(SSD1306_NORMALDISPLAY);

			Dis_command(SSD1306_DISPLAYON);//--turn on oled panel
			
			if ((DisSetting[Display].DispInvers&0x01)==1) Dis_command(SSD1306_INVERTDISPLAY);
			Dis_dim(DisSetting[Display].DispContrast);
			Dis_clearDisplay();
			Dis_display(false);
		}
	SetActiveDisplay(tempDis,true);
}

void TurnOffDisplay(uint8_t Display)
{
	uint8_t tempDisplay = ActiveDisplay;
		if (DisSetting[Display].Active!=0)
		{	
			SetActiveDisplay(Display,true);
			Dis_command(SSD1306_DISPLAYOFF);	
		}
	SetActiveDisplay(tempDisplay,true);
}

void TurnOffAllDisplays()
{
	for (uint8_t i=0; i<4;i++)
	{
		TurnOffDisplay(i);
	}
}

void Dis_clearDisplay(void) 
{
	SetActiveLine(0);
	SetActiveSection(0);
	DisSetting[ActiveDisplay].xBlinkStart=0;
	DisSetting[ActiveDisplay].xBlinkEnd=0;
	DisSetting[ActiveDisplay].BlinkLine=0;
	DisSetting[ActiveDisplay].AltPos=0;
	DisSetting[ActiveDisplay].AltPos1=0;
	DisSetting[ActiveDisplay].BlinkState=0;
	memset(DisSetting[ActiveDisplay].buffer,0,192);
	if (DisSetting[ActiveDisplay].ShowClock==1) SetClock(lastHour,lastMinute,false);
	SetupCon=0;
}

void Dis_invertDisplay(uint8_t i) 
{
	if (DisSetting[ActiveDisplay].Active!=0)
	{
		if (i==1)
		{
			Dis_command(SSD1306_INVERTDISPLAY);
		} else 
		{
			Dis_command(SSD1306_NORMALDISPLAY);
		}
	}
}

void Dis_display(bool isClockSet) 
{
	// TODO: Update only changed section i.e if clockset only first 20 Cols, if Line0 then only Page0, if line1 only page 1

	if (DisSetting[ActiveDisplay].Active==1)
	{
		i2c_init();
				
		Dis_command(SSD1306_COLUMNADDR);
		Dis_command(0);   // Column start address (0 = reset)
		Dis_command(DispWidth-1); // Column end address (127 = reset)

		Dis_command(SSD1306_PAGEADDR);
		Dis_command(0); // Page start address (0 = reset)
		Dis_command(1); // Page end address
				
		for (uint16_t i=0; i<(DispWidth*DispHeight/(uint16_t)8); i++)
		{
			if (!i2c_start((Display_Address<<1) | I2C_WRITE )) return;
			i2c_write(0x40);
			for (uint8_t x=0; x<16; x++)
			{
				i2c_write(DisSetting[ActiveDisplay].buffer[i]);
				i++;
			}
			i--;
			i2c_stop();
		}
	}
}

void Dis_dim(uint8_t dim) 
{
	if (DisSetting[ActiveDisplay].Active!=0)
	{
		Dis_command(SSD1306_SETCONTRAST);
		Dis_command(dim);
	}
}

void drawPixel(uint16_t x, uint16_t y, uint16_t color) 
{
	if ((x < 0) || (x >= _width[ActiveDisplay]) || (y < 0) || (y >= _height[ActiveDisplay]))
	return;

	// check rotation, move pixel around if necessary
	switch (DisSetting[ActiveDisplay].Rotate) {
		case 1:
		swap(x, y);
		x = DispWidth - x - 1;
		break;
		case 2:
		x = DispWidth - x - 1;
		y = DispHeight - y - 1;
		break;
		case 3:
		swap(x, y);
		y = DispHeight - y - 1;
		break;
	}

	// x is which column
	uint8_t tmpVal =0;
	uint16_t tmpAddr;
	tmpAddr = (x + ((y/8)*DispWidth));
	
	switch (color)
	{
		case WHITE:
			tmpVal = DisSetting[ActiveDisplay].buffer[tmpAddr];
			tmpVal |= (1 << (y&7));
			DisSetting[ActiveDisplay].buffer[tmpAddr]=tmpVal;
		break;
		case BLACK:
			tmpVal = DisSetting[ActiveDisplay].buffer[tmpAddr];
			tmpVal &= ~(1 << (y&7));
			DisSetting[ActiveDisplay].buffer[tmpAddr]=tmpVal;
		break;
		case INVERSE:
			tmpVal = DisSetting[ActiveDisplay].buffer[tmpAddr];
			tmpVal ^= (1 << (y&7));
			DisSetting[ActiveDisplay].buffer[tmpAddr]=tmpVal;
		break;
	}
}

void drawLine(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t color) 
{
	uint16_t steep = abs(y1 - y0) > abs(x1 - x0);
	if (steep) 
	{
		swap(x0, y0);
		swap(x1, y1);
	}

	if (x0 > x1) 
	{
		swap(x0, x1);
		swap(y0, y1);
	}

	uint16_t dx, dy;
	dx = x1 - x0;
	dy = abs(y1 - y0);

	int16_t err = dx / 2;
	uint16_t ystep;

	if (y0 < y1) 
	{
		ystep = 1;
	} 
	else 
	{
		ystep = -1;
	}

	for (; x0<=x1; x0++) 
	{
		if (steep) 
		{
			drawPixel(y0, x0, color);
		} 
		else 
		{
			drawPixel(x0, y0, color);
		}
		err -= dy;
		if (err < 0) 
		{
			y0 += ystep;
			err += dx;
		}
	}
}

void fillCircle(uint16_t x0, uint16_t y0, uint16_t r, uint16_t color) 
{
	drawFastVLine(x0, y0-r, 2*r+1, color);
	fillCircleHelper(x0, y0, r, 3, 0, color);
}

// Used to do circles and roundrects
void fillCircleHelper(int16_t x0, int16_t y0, int16_t r, uint8_t cornername, int16_t delta, uint16_t color) 
{

	int16_t f     = 1 - r;
	int16_t ddF_x = 1;
	int16_t ddF_y = -2 * r;
	int16_t x     = 0;
	int16_t y     = r;

	while (x<y) 
	{
		if (f >= 0) 
		{
			y--;
			ddF_y += 2;
			f     += ddF_y;
		}
		x++;
		ddF_x += 2;
		f     += ddF_x;

		if (cornername & 0x1) 
		{
			drawFastVLine(x0+x, y0-y, 2*y+1+delta, color);
			drawFastVLine(x0+y, y0-x, 2*x+1+delta, color);
		}
		if (cornername & 0x2) 
		{
			drawFastVLine(x0-x, y0-y, 2*y+1+delta, color);
			drawFastVLine(x0-y, y0-x, 2*x+1+delta, color);
		}
	}
}

void drawFastVLine(int16_t x, int16_t y, int16_t h, uint16_t color) 
{
	bool bSwap = false;
	switch(DisSetting[ActiveDisplay].Rotate) 
	{
		case 0:
		break;
		case 1:
			// 90 degree rotation, swap x & y for rotation, then invert x and adjust x for h (now to become w)
			bSwap = true;
			swap(x, y);
			x = DispWidth - x - 1;
			x -= (h-1);
			break;
		case 2:
			// 180 degree rotation, invert x and y - then shift y around for height.
			x = DispWidth - x - 1;
			y = DispHeight - y - 1;
			y -= (h-1);
			break;
		case 3:
			// 270 degree rotation, swap x & y for rotation, then invert y
			bSwap = true;
			swap(x, y);
			y = DispHeight - y - 1;
		break;
	}

	if(bSwap) {
		drawFastHLineInternal(x, y, h, color);
		} else {
		drawFastVLineInternal(x, y, h, color);
	}
}

void drawFastVLineInternal(int16_t x, int16_t __y, int16_t __h, uint16_t color)
{
	
	// do nothing if we're off the left or right side of the screen
	if(x < 0 || x >= (int16_t)DispWidth) { return; }

	// make sure we don't try to draw below 0
	if(__y < 0) {
		// __y is negative, this will subtract enough from __h to account for __y being 0
		__h += __y;
		__y = 0;

	}

	// make sure we don't go past the height of the display
	if( (__y + __h) > (int16_t)DispHeight) {
		__h = (DispHeight - __y);
	}

	// if our height is now negative, punt
	if(__h <= 0) {
		return;
	}

	// this display doesn't need ints for coordinates, use local byte registers for faster juggling
	register uint8_t y = __y;
	register uint8_t h = __h;

	//uint8_t bb[192];
	//for (uint8_t pp = 0;pp<192;pp++)
	//	bb[pp]=buffer[pp][ActiveDisplay];
	// set up the pointer for fast movement through the buffer
	register uint8_t *pBuf = DisSetting[ActiveDisplay].buffer;

	// adjust the buffer pointer for the current row
	pBuf += ((y/8) * DispWidth);
	// and offset x columns in
	pBuf += x;

	// do the first partial byte, if necessary - this requires some masking
	register uint8_t mod = (y&7);
	if(mod) {
		// mask off the high n bits we want to set
		mod = 8-mod;

		// note - lookup table results in a nearly 10% performance improvement in fill* functions
		// register uint8_t mask = ~(0xFF >> (mod));
		static uint8_t premask[8] = {0x00, 0x80, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC, 0xFE };
		register uint8_t mask = premask[mod];

		// adjust the mask if we're not going to reach the end of this byte
		if( h < mod) {
			mask &= (0XFF >> (mod-h));
		}

		switch (color)
		{
			case WHITE:   *pBuf |=  mask;  break;
			case BLACK:   *pBuf &= ~mask;  break;
			case INVERSE: *pBuf ^=  mask;  break;
		}
		
		// fast exit if we're done here!
		if(h<mod) { return; }

		h -= mod;

		pBuf += DispWidth;
	}


	// write solid bytes while we can - effectively doing 8 rows at a time
	if(h >= 8) {
		if (color == INVERSE)  {          // separate copy of the code so we don't impact performance of the black/white write version with an extra comparison per loop
			do  {
				*pBuf=~(*pBuf);

				// adjust the buffer forward 8 rows worth of data
				pBuf += DispWidth;

				// adjust h & y (there's got to be a faster way for me to do this, but this should still help a fair bit for now)
				h -= 8;
			} while(h >= 8);
		}
		else {
			// store a local value to work with
			register uint8_t val = (color == WHITE) ? 255 : 0;

			do  {
				// write our value in
				*pBuf = val;

				// adjust the buffer forward 8 rows worth of data
				pBuf += DispWidth;

				// adjust h & y (there's got to be a faster way for me to do this, but this should still help a fair bit for now)
				h -= 8;
			} while(h >= 8);
		}
	}

	// now do the final partial byte, if necessary
	if(h) {
		mod = h & 7;
		// this time we want to mask the low bits of the byte, vs the high bits we did above
		// register uint8_t mask = (1 << mod) - 1;
		// note - lookup table results in a nearly 10% performance improvement in fill* functions
		static uint8_t postmask[8] = {0x00, 0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3F, 0x7F };
		register uint8_t mask = postmask[mod];
		switch (color)
		{
			case WHITE:   *pBuf |=  mask;  break;
			case BLACK:   *pBuf &= ~mask;  break;
			case INVERSE: *pBuf ^=  mask;  break;
		}
	}
}

void drawFastHLine(int16_t x, int16_t y, int16_t w, uint16_t color) 
{
	
	
	bool bSwap = false;
	switch(DisSetting[ActiveDisplay].Rotate) {
		case 0:
		// 0 degree rotation, do nothing
		break;
		case 1:
		// 90 degree rotation, swap x & y for rotation, then invert x
		bSwap = true;
		swap(x, y);
		x = DispWidth - x - 1;
		break;
		case 2:
		// 180 degree rotation, invert x and y - then shift y around for height.
		x = DispWidth - x - 1;
		y = DispHeight - y - 1;
		x -= (w-1);
		break;
		case 3:
		// 270 degree rotation, swap x & y for rotation, then invert y  and adjust y for w (not to become h)
		bSwap = true;
		swap(x, y);
		y = DispHeight - y - 1;
		y -= (w-1);
		break;
	}

	if(bSwap) {
		drawFastVLineInternal(x, y, w, color);
		} else {
		drawFastHLineInternal(x, y, w, color);
	}
}

void drawFastHLineInternal(int16_t x, int16_t y, int16_t w, uint16_t color) 
{
	
	// Do bounds/limit checks
	if(y < 0 || y >= (int16_t)DispHeight) { return; }

	// make sure we don't try to draw below 0
	if(x < 0) {
		w += x;
		x = 0;
	}

	// make sure we don't go off the edge of the display
	if( (x + w) > (int16_t)DispWidth) {
		w = (DispWidth - x);
	}

	// if our width is now negative, punt
	if(w <= 0) { return; }

	register uint8_t *pBuf = DisSetting[ActiveDisplay].buffer;
	// adjust the buffer pointer for the current row
	pBuf += ((y/8) * DispWidth);
	// and offset x columns in
	pBuf += x;

	register uint8_t mask = 1 << (y&7);

	switch (color)
	{
		case WHITE:         while(w--) { *pBuf++ |= mask; }; break;
		case BLACK: mask = ~mask;   while(w--) { *pBuf++ &= mask; }; break;
		case INVERSE:         while(w--) { *pBuf++ ^= mask; }; break;
	}
}

void drawBox (int16_t x, int16_t y, int16_t w, int16_t h ,uint16_t color)
{
	if ((x+w)>=(int16_t)DispWidth) w=DispWidth-x;
	if (x<0) x=0;
	if (y<0) y=0;
	if ((y+h)>=(int16_t)DispHeight) h=DispHeight-y;
	for (uint8_t i = x; i<x+w;i++)
	{
		drawFastVLine(i,y,h,color);
	}
}

void setRotation(uint8_t x,uint8_t Display) 
{
	uint8_t rotation = (x & 3);
	switch(rotation) 
	{
		case 0:
		case 2:
		_width[Display]  = DispWidth;
		_height[Display] = DispHeight;
		break;
		case 1:
		case 3:
		_width[Display]  = DispHeight;
		_height[Display] = DispWidth;
		break;
	}
	DisSetting[ActiveDisplay].Rotate=rotation;
	Fram_WriteByte(((Display*EE_CntDisSetting)+EE_OffsetDis+EE_DispRotate),rotation);
	
}

void doRotation(uint8_t x,uint8_t Display)
{
	uint8_t rotation = (x & 3);
	switch(rotation)
	{
		case 0:
		case 2:
		_width[Display]  = DispWidth;
		_height[Display] = DispHeight;
		break;
		case 1:
		case 3:
		_width[Display]  = DispHeight;
		_height[Display] = DispWidth;
		break;
	}
}

void writeBMP(uint8_t BMPNR)
{
	Fram_ReadSmallBMP(BMPNR);
	drawChar(DisSetting[ActiveDisplay].textcolor);
	
	if (DisSetting[ActiveDisplay].activeSection==0)
	{
		if (cursor_x[ActiveDisplay][DisSetting[ActiveDisplay].activeLine]>=DisSetting[ActiveDisplay].TxtMax)
		cursor_x[ActiveDisplay][DisSetting[ActiveDisplay].activeLine]=DisSetting[ActiveDisplay].TxtMax;
	}
	else
	{
		if (cursor_x[ActiveDisplay][DisSetting[ActiveDisplay].activeLine]>=DisSetting[ActiveDisplay].TxtTimesMax)
		cursor_x[ActiveDisplay][DisSetting[ActiveDisplay].activeLine]=DisSetting[ActiveDisplay].TxtTimesMax;
	}
	
}
 
void drawChar(uint8_t color) 
{
	uint8_t bg=0;
	if (color==BLACK) bg = WHITE;
	if (color==WHITE) bg = BLACK;
	
	if (DisSetting[ActiveDisplay].activeSection==1)
	{
		if (cursor_x[ActiveDisplay][DisSetting[ActiveDisplay].activeLine] >= DisSetting[ActiveDisplay].TxtTimesMax) return;
	}
	else
	{
		if (cursor_x[ActiveDisplay][DisSetting[ActiveDisplay].activeLine] >= DisSetting[ActiveDisplay].TxtMax) return;
	}

	for (uint8_t i=0; i<RamBufferSize; i++ ) 
	{
		uint8_t line;
		line = RamBuffer[i];
		if (DisSetting[ActiveDisplay].activeSection==1)
		{
				if ((cursor_x[ActiveDisplay][DisSetting[ActiveDisplay].activeLine]) >= DisSetting[ActiveDisplay].TxtTimesMax)
				{
					cursor_x[ActiveDisplay][DisSetting[ActiveDisplay].activeLine]= DisSetting[ActiveDisplay].TxtTimesMax;
					return;
				}
		}
		else
		{
			if ((cursor_x[ActiveDisplay][DisSetting[ActiveDisplay].activeLine]) >= DisSetting[ActiveDisplay].TxtMax)
			{
				cursor_x[ActiveDisplay][DisSetting[ActiveDisplay].activeLine]= DisSetting[ActiveDisplay].TxtMax;
				return;
			}
		}
		for (uint8_t j = 0; j<8; j++) 
		{
			if (line & 0x1) 
			{
				drawPixel(cursor_x[ActiveDisplay][DisSetting[ActiveDisplay].activeLine], cursor_y[ActiveDisplay]+j, color);
			} 
			else if (bg != color) 
			{
				drawPixel(cursor_x[ActiveDisplay][DisSetting[ActiveDisplay].activeLine], cursor_y[ActiveDisplay]+j, bg);
			}
			line >>= 1;
		}
		cursor_x[ActiveDisplay][DisSetting[ActiveDisplay].activeLine]++;
	}
}

void write(uint8_t c) 
{
	if (((c > 0x1f) & (c < 0x7f)) || (c > 0xa0))	// Draw char
	{
		Fram_ReadFont(c,GetFont());
		drawChar(DisSetting[ActiveDisplay].textcolor);
	}
	
}

void ResetX(uint8_t Line)
{
	if (DisSetting[ActiveDisplay].activeSection==0)
		cursor_x[ActiveDisplay][DisSetting[ActiveDisplay].activeLine]=DisSetting[ActiveDisplay].TxtStart;
	else
		cursor_x[ActiveDisplay][DisSetting[ActiveDisplay].activeLine]=DisSetting[ActiveDisplay].TxtTmimesStart;
}

void ClearLine(uint8_t Line)
{
	if (DisSetting[ActiveDisplay].activeSection==0)
	{
		for (uint8_t i = DisSetting[ActiveDisplay].TxtStart;i< DisSetting[ActiveDisplay].TxtMax;i++)
		{
			for (uint8_t j = (8*Line);j<((8*Line)+8);j++)
			drawPixel(i,j,BLACK);
		}
	}
	else
	{
		for (uint8_t i = DisSetting[ActiveDisplay].TxtTmimesStart;i< DisSetting[ActiveDisplay].TxtTimesMax;i++)
		{
			for (uint8_t j = (8*Line);j<((8*Line)+8);j++)
			drawPixel(i,j,BLACK);
		}
	}
	ResetX(Line);
}



