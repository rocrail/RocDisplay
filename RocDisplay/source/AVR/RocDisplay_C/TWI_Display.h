/* 
* TWI_Display.h
*
Class Header for 96x16 Display with SSD1306 and Fonts,Bitmaps and strings stored in F-Ram.

�2015 Walter Sax

YOU CAN USE THIS CODE FOR YOUR PRIVATE USE OR ANY OTHER USE
AS LONG AS THIS SOFTWARE IF FRRE OF CHARGE AND THE PRODUCTS INCLUDING THIS SOFTWARE ARE ALSO FREE OF CHARGE AND THIS DISCLAIMER IS INCLUDED .
ANY COMMERCIAL USE OF THIS CODE INCLUDED IN DEVICES OR COMMERCIAL DISTRIBUTION WITHOUT MY PERMISSION IS STRICTLY FORBITTEN.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
	
	
*/



#ifndef __TWI_DISPLAY_H__
#define __TWI_DISPLAY_H__

#include <inttypes.h>
#include <stdio.h>
#include <avr/pgmspace.h>

#include <stdbool.h>

// TWI Addresses
#define Fram_Address 0x50
#define Display_Address 0x3C
#define Switch_Address 0x70

// FRAM Positions
#define AddrSmallBMP (0UL + ((uint16_t)BmpNr * 21UL))
//#define AddrChrFont (630UL +((((uint16_t)Chr < 0x007F ? (uint16_t)Chr-0x0020UL:(uint16_t)Chr-65UL)*7UL)+ ((uint16_t)Font * 224UL *7UL)))
#define AddrChrFont (420UL + ((((uint16_t)Chr-0x20UL)*7UL)+ ((uint16_t)Font * 224UL *7UL))  )
#define AddrTxtBlock (6692UL + (TxtNo*31UL))
#define AddrBlinBuff (7312 + ((ActiveDisplay*194)+(DisSetting[ActiveDisplay].BlinkState&0x01)*97))

// Time Retuns from Function
#define ReturnX 0
#define ReturnY 1
#define ReturnMinute 0
#define ReturnHour 1

#define ClockWidth 15

// Display
#define BLACK 0
#define WHITE 1
#define INVERSE 2    // --> DO NOT USE IT - I IGNORED INVERS USE BLACK OR WHITE OR SET DISPLAY TO INVERT

#define SSD1306_SETCONTRAST 0x81
#define SSD1306_DISPLAYALLON_RESUME 0xA4
#define SSD1306_DISPLAYALLON 0xA5
#define SSD1306_NORMALDISPLAY 0xA6
#define SSD1306_INVERTDISPLAY 0xA7
#define SSD1306_DISPLAYOFF 0xAE
#define SSD1306_DISPLAYON 0xAF
#define SSD1306_SETDISPLAYOFFSET 0xD3
#define SSD1306_SETCOMPINS 0xDA
#define SSD1306_SETVCOMDETECT 0xDB
#define SSD1306_SETDISPLAYCLOCKDIV 0xD5
#define SSD1306_SETPRECHARGE 0xD9
#define SSD1306_SETMULTIPLEX 0xA8
#define SSD1306_SETLOWCOLUMN 0x00
#define SSD1306_SETHIGHCOLUMN 0x10
#define SSD1306_SETSTARTLINE 0x40
#define SSD1306_MEMORYMODE 0x20
#define SSD1306_COLUMNADDR 0x21
#define SSD1306_PAGEADDR   0x22
#define SSD1306_COMSCANINC 0xC0
#define SSD1306_COMSCANDEC 0xC8
#define SSD1306_SEGREMAP 0xA0
#define SSD1306_CHARGEPUMP 0x8D
#define SSD1306_EXTERNALVCC 0x1
#define SSD1306_SWITCHCAPVCC 0x2

// Scrolling #defines
#define SSD1306_ACTIVATE_SCROLL 0x2F
#define SSD1306_DEACTIVATE_SCROLL 0x2E
#define SSD1306_SET_VERTICAL_SCROLL_AREA 0xA3
#define SSD1306_RIGHT_HORIZONTAL_SCROLL 0x26
#define SSD1306_LEFT_HORIZONTAL_SCROLL 0x27
#define SSD1306_VERTICAL_AND_RIGHT_HORIZONTAL_SCROLL 0x29
#define SSD1306_VERTICAL_AND_LEFT_HORIZONTAL_SCROLL 0x2A

#define RST_DDR		DDRB
#define RST_PIN		PINB
#define RST_PORT	PORTB
#define RST_NO		PORTB0
#define RST_INI		RST_DDR |= (1<<RST_NO)
#define RST_SET (RST_PORT |= (1<<RST_NO))
#define RST_CLR (RST_PORT &= ~(1<<RST_NO))

#define swap(a, b) { int16_t t = a; a = b; b = t; }
	
#define EE_DispWidth		8088
#define EE_DispHeight		8089
#define EE_DispCnt			8090

#define EE_DispClkShow		0					//8091	8102	8113	8124
#define EE_DispClkSide		1					//8092	8103	8114	8125
#define EE_DispSync			2					//8093	8104	8115	8126
#define EE_DispInvers		3					//8094	8105	8116	8127
#define EE_DispContrast		4					//8095	8106	8117	8128
#define EE_DispDepCol		5					//8096	8107	8118	8129
#define EE_DispDepColWidth	6					//8097	8108	8119	8130
#define EE_DispRotate		7					//8098	8109	8120	8131
#define EE_Blinktime		8					//8099	8110	8121	8132
#define EE_DispFont1		9					//8100	8111	8122	8133
#define EE_DispFont2		10					//8101	8112	8123	8134
#define EE_OffsetDis		8091

#define EE_CntDisSetting	11
#define EE_FirstStart		8136

#define TimerPreLoad		0xE17B

struct tDisSetting
{
	uint8_t Active;
	uint8_t ShowClock;
	uint8_t ClockSide;
	uint8_t DispSync;
	uint8_t DispInvers;
	uint8_t DispContrast;
	uint8_t ShowDepCol;
	uint8_t DepColWidth;
	uint8_t Rotate;
	uint8_t ClockX;
	uint8_t TxtStart;
	uint8_t TxtMax;
	uint8_t TxtTmimesStart;
	uint8_t TxtTimesMax;
	uint8_t textcolor;
	uint8_t activeLine;
	uint8_t activeSection;
	uint8_t buffer[192];
	uint8_t Font[2];
	uint8_t xBlinkStart;
	uint8_t xBlinkEnd;
	uint8_t BlinkLine;
	uint8_t BlinkTime;
	uint8_t AltPos;
	uint8_t AltPos1;
	uint8_t BlinkState;
} DisSetting[4];

	uint8_t RamBuffer[96];
	uint8_t RamBufferSize;
	uint16_t DispWidth;
	uint16_t DispHeight;
	uint8_t ConDisplays;
	uint8_t ActiveDisplay;  // 0=First / 1=Second....
	uint8_t SetupCon;
	uint8_t BlinkCnt[4];
	
	uint16_t _width[4], _height[4]; // Display w/h as modified by current rotation
	uint16_t cursor_x[4] [2];
	uint16_t cursor_y[4];
	uint8_t lastHour;
	uint8_t lastMinute;

	void TWI_Display();
	
	void StartDisplays();
	
	bool StartSoftI2C();
	uint8_t I2CTest();
	
	//SETTINGS
	void GetSettings();
	void SetDisplaySize(uint8_t Width, uint8_t Height);
	uint8_t GetDisplayWidth();
	uint8_t GetDisplayHeight();
	void SetDisplayCount(uint8_t Count);
	uint8_t isActive();
	void SetSyncDisplays(uint8_t DispSync);
	void SetDisplayClock(uint8_t ClkDisp,uint8_t Display);
	void SetClockSide(char Side,uint8_t Display);
	void SetDisplayInvert(uint8_t Invert,uint8_t Display);
	void SetKontrast(uint8_t Kontrast,uint8_t Display);
	void SetActiveLine(uint8_t Line);
	void SetActiveSection(uint8_t Section);
	void SetShowDepartureCol(uint8_t Show,uint8_t Display);
	void SetDepartureCol(uint8_t ColWidth, uint8_t Display);
	void SetActiveDisplay(uint8_t Display,bool SetMux);
	void SetFont(uint8_t font);
	uint8_t GetFont();
	void BlinkStart ();
	void BlinkEnd ();
	void DoBlink();
	void BlinkTimerStop();
	void BlinkTimerStart();
	void SetBlinkTime(uint8_t BlkTime,uint8_t Display);
	void WriteAlternate(uint8_t c,uint8_t bf);
	void AlternateBMP(uint8_t Nr,uint8_t bf);
	
	void SetCsr_X(uint8_t x);
	void SetCsr_Y(uint8_t y);
	
	void SetClock(uint8_t hour,uint8_t minute,bool redraw);
	
	//FRAM
	
	bool Fram_WriteByte(uint16_t Address,uint8_t byte);
	uint8_t Fram_ReadByte (uint16_t Address);
	
	bool Fram_WriteSmallBMP(uint8_t BmpNr);
	bool Fram_ReadSmallBMP(uint8_t BmpNr);
	bool Fram_WriteBigBMP(uint8_t BigBmpNr);
	bool Fram_ReadBigBMP(uint8_t BigBmpNr);
	bool Fram_WriteFont(char Chr,uint8_t Font);
	bool Fram_ReadFont(char Chr,uint8_t Font);
	bool Fram_SaveTxt(uint8_t *txt,uint8_t TxtNo);
	bool Fram_LoadTxt(uint8_t TxtNo);
	
	// Display
	void Dis_command(uint8_t c);
	void Dis_data(uint8_t c);
	void Dis_StartDisplay();
	void StartSingleDisplay(uint8_t Display);
	void TurnOffDisplay(uint8_t Display);
	void TurnOffAllDisplays();
	void Dis_clearDisplay(void);
	void Dis_invertDisplay(uint8_t i);
	void Dis_display(bool isClockSet);
	void Dis_dim(uint8_t dim);
	
	void writeBMP(uint8_t BMPNR);
	void write(uint8_t c);
	void ResetX(uint8_t Line);
	void ClearLine(uint8_t Line);
	
	void setRotation(uint8_t x, uint8_t Display);
	
	bool SetFactoryReset(uint8_t sp);
	
	void SetOffset(unsigned char disp);
	
	int8_t GetXYFromTime(double Minute, double Hour, uint8_t WhatReturn, uint8_t WantXY);
	int8_t GetXFromPolar(double deg, double rad);
	int8_t GetYFromPolar(double deg, double rad);
	
	void doRotation(uint8_t x, uint8_t Display);
	
	void drawPixel(uint16_t x, uint16_t y, uint16_t color);
	void drawLine(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t color);
	void fillCircle(uint16_t x0, uint16_t y0, uint16_t r, uint16_t color);
	void fillCircleHelper(int16_t x0, int16_t y0, int16_t r, uint8_t cornername, int16_t delta, uint16_t color);
	void drawFastVLine(int16_t x, int16_t y, int16_t h, uint16_t color);
	void drawFastVLineInternal(int16_t x, int16_t __y, int16_t __h, uint16_t color);
	void drawFastHLine(int16_t x, int16_t y, int16_t w, uint16_t color); 
	void drawFastHLineInternal(int16_t x, int16_t y, int16_t w, uint16_t color);
	void drawBox (int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color);
	void drawSmallBitmap(int16_t x, int16_t y, uint16_t color);
	void drawChar(uint8_t color);
	
	void Fram_WriteBlinbuffer(uint8_t pos);
	void Fram_ReadBlinkBuffer();
	void Fram_ClearBlinkBuffer();
	
	void ResetI2CMux();

#endif //__TWI_DISPLAY_H__
