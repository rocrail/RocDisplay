/*
	RocDisplay_C.c
 
 	RocDisplay is a Software and Hardware Displaydriver for 96x16 Displays to be used for model railways.
		
	
 
		�2015 Walter Sax

		YOU CAN USE THIS CODE FOR YOUR PRIVATE USE OR ANY OTHER USE
		AS LONG AS THIS SOFTWARE IF FRRE OF CHARGE AND THE PRODUCTS INCLUDING THIS SOFTWARE ARE ALSO FREE OF CHARGE AND THIS DISCLAIMER IS INCLUDED .
		ANY COMMERCIAL USE OF THIS CODE INCLUDED IN DEVICES OR COMMERCIAL DISTRIBUTION WITHOUT MY PERMISSION IS STRICTLY FORBITTEN.

		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
		AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
		IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
		ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
		LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
		CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
		SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
		INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
		CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
		ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
		POSSIBILITY OF SUCH DAMAGE.


 */ 

// Led IO's 

#define Led_Ge_On		PORTB |= (1<<PORTB2)
#define Led_Ge_Off		PORTB &= ~(1<<PORTB2)
#define Led_Ge_Tgl		PORTB ^= (1<<PORTB2)

#define Led_Rt_On		PORTB |= (1<<PORTB1)
#define Led_Rt_Off		PORTB &= ~(1<<PORTB1)
#define Led_Rt_Tgl		PORTB ^= (1<<PORTB1)

// Defs for I2C Read Registers
#define Reg_ShowClock			0x01		//1Data: ShowClock 0..1
#define Reg_ClockSide			0x02		//1Data: ClockSide (0-Left,1-Right)
#define Reg_Rotate				0x03		//1Data: ClockSide (0-Left,1-Right)
#define Reg_Invert				0x04		//1Data: Invert 0..1
#define Reg_DisplayCount		0x05		//1Data: 1..2
#define Reg_SyncDisplays		0x06		//1Data: 0..1 (1 Only if DisplayCount>1)
#define Reg_ShowDepCol			0x07		//1Data: 0..1 (Show Depature Column)
#define Reg_DepColWidth			0x08		//1Data: 0..95 Width of Departure Column
#define Reg_ActiveDisplay		0x09
#define Reg_SmallBmp			0x0A		//*B1: Nr -B2: Width -B3-B12: 10Byte BMP
#define Reg_Font				0x0B		//*B1: Nr *B2: Char -B3 Width -B4-B11 FontData
#define Reg_TextTemp			0x0C
#define Reg_Contrast			0x0D

#define Reg_EnterBootloader		0x0F		//1Data: 0xFF

#define Reg_Blinktime			0x10



//ACK after received Data/ ACK await after sended Data
#define TWCR_ACK 	TWCR = (1<<TWEN)|(1<<TWIE)|(1<<TWINT)|(1<<TWEA)|(0<<TWSTA)|(0<<TWSTO)|(0<<TWWC);

//NACK after received/ NACK await after send Data
#define TWCR_NACK 	TWCR = (1<<TWEN)|(1<<TWIE)|(1<<TWINT)|(0<<TWEA)|(0<<TWSTA)|(0<<TWSTO)|(0<<TWWC);

//I2C Register to unaddresed Slave mode init.
#define TWCR_RESET 	TWCR = (1<<TWEN)|(1<<TWIE)|(1<<TWINT)|(1<<TWEA)|(0<<TWSTA)|(0<<TWSTO)|(0<<TWWC);

#include <avr/io.h>
#include "TWI_Display.h"
#include <string.h>
#include <avr/interrupt.h>
#include <util/twi.h>
#include <util/delay.h>

//	I2C MessageBuffer
volatile uint8_t RR_Buffer[128];
//	I2C BufferCount
volatile uint8_t RR_BuffPt;

// I2C ISR Var.
//volatile uint8_t bcnt;
volatile uint8_t intReg;
volatile uint8_t tmp1,tmp2,tmp3;
volatile uint8_t data;
volatile uint8_t Register;
volatile uint8_t AcDispComm;

// Bootloader Definition Address
typedef void (*AppPtr_t)(void) __attribute__((noreturn));
const AppPtr_t bootloader = (AppPtr_t)0x3800;    // --> 4kB Bootloader (2048w) --> 0x3C00 @ 2kB Bootloader

// Programm Restart
void (*start)( void ) = 0x0000;


// Hex to INT Conversation
uint16_t hex2num(uint8_t *s,uint8_t size)
{
	uint16_t x = 0;
	for(uint8_t z=0;z<size;z++) {
		char c = *s;
		if (c >= '0' && c <= '9') {
			x *= 16;
			x += c - '0';
		}
		else if (c >= 'A' && c <= 'F') {
			x *= 16;
			x += (c - 'A') + 10;
		}
		else break;
		s++;
	}
	return x;
}

// I2C Interrupt
ISR(TWI_vect)
{
	//BlinkTimerStop();
	cli();
static uint8_t bcnt;
static uint8_t bcnt1;
static uint8_t inData;
//static uint8_t intReg;
//static uint8_t tmp1,tmp2,tmp3;
//static uint8_t data;
//static uint8_t Register;
	
	switch ((TW_STATUS)) 
	{
		case TW_SR_SLA_ACK:  // SLA + W Received - ACK Ret
			Led_Rt_On;
			// Reset all Variables -- Why it arrives here if I got SLA+R - don't know - do not clear the commented Vars.
			bcnt = 0;
			//Register=0;
			//intReg=0;
			//tmp1=0;
			//tmp2=0;
			memset((void*)RR_Buffer,0,sizeof(RR_Buffer));
			TWCR_ACK;
			break;
		
		/* prev. SLA+W, data received, ACK returned -> receive data and ACK */
		case TW_SR_DATA_ACK:
			
			inData = TWDR;
			// First Byte is DisplayNummber..... Handle Only between 1 and 4 - For ClockSync >4 can also be used
			if (bcnt==0) 
			{
				AcDispComm=inData;
				
			}
			else	// Put byte in In Buffer - Pharse and set Flags also for Reading - Have to be done here inside the ISR 
			{
				RR_Buffer[bcnt]=inData;	
				
				if ((Register & 0x01)==0)
				{
					if (RR_Buffer[bcnt]==123)
						Register |= 1;
				}
				else if (((RR_Buffer[bcnt]=='Q') & ((Register & 0x01) == 1)))
				{
						Register |= 2;
				}
				else if (((intReg==0) & ((Register & 0x03)==3)))
				{
					switch (RR_Buffer[bcnt])
					{
						case 125:
							Register&=~(1);
							break;
						case 'A':
							intReg = Reg_ActiveDisplay;
							break;
						case 'S':
							intReg = Reg_ClockSide;
							break;
						case 'I':
							intReg = Reg_Invert;
							break;
						case 'Z':
							intReg = Reg_DisplayCount;
							break;
						case 'V':
							intReg = Reg_SyncDisplays;
							break;
						case 'D':
							intReg = Reg_ShowDepCol;
							break;
						case 'W':
							intReg = Reg_DepColWidth;
							break;
						case 'R':
							intReg = Reg_Rotate;
							break;
						case 'H':
							intReg = Reg_Contrast;
							break;
						case 'G':
							intReg = Reg_TextTemp;
							Register |= 0x10;
							break;
						case 'M':
							intReg = Reg_SmallBmp;
							Register |= 0x04;
							break;
						case  'N':
							intReg = Reg_Font;
							Register |= 0x08;
							break;
						case 'J':
							intReg = Reg_Blinktime;
							break;
					}
				}
				else if ((intReg==Reg_SmallBmp) & (tmp3<=2))
				{
					if ((RR_Buffer[bcnt]>47) & (RR_Buffer[bcnt]<58))
					{
						if (tmp3==0)
						{
							tmp1 = (RR_Buffer[bcnt]-48);
							tmp3=1;
						}
						else if (tmp3==1)
						{
							tmp1=tmp1 * 10;
							tmp1+=(RR_Buffer[bcnt]-48);
							tmp3=3;
							Register = 0x03;
						}
					}
					else if (RR_Buffer[bcnt]==59)
					{
						tmp3=3;
						Register=0x03;
					}
				
				}
				else if ((intReg==Reg_Font) & (tmp3==0))
				{
					tmp1 = RR_Buffer[bcnt]-48;
					tmp3=1;
				}
				else if ((intReg==Reg_Font) & (tmp3<5))
				{
					if ((RR_Buffer[bcnt]>47) & (RR_Buffer[bcnt]<58))
					 {
						if (tmp3==1)
						{
							tmp2 = RR_Buffer[bcnt]-48;
							tmp3=2;
						}
						else if (tmp3==2)
						{
							tmp2= tmp2 * 10;
							tmp2+=RR_Buffer[bcnt]-48;
							tmp3=3;
						}
						else if (tmp3==3)
						{
							tmp2 = tmp2 * 10;
							tmp2+=RR_Buffer[bcnt]-48;
							tmp3=4;	
						}
					 }
					 else if (RR_Buffer[bcnt]==59)
					 {
						 tmp3=5;
						 Register=0x03;
					 }
				}
				else if ((intReg==Reg_TextTemp) & (tmp3<2))
				{
					if ((RR_Buffer[bcnt]>47) & (RR_Buffer[bcnt]<58))
					{
						if (tmp3==0)
						{
							tmp1 = (RR_Buffer[bcnt]-48);
							tmp3=1;
						}
						else if (tmp3==1)
						{
							tmp1=tmp1 * 10;
							tmp1+=(RR_Buffer[bcnt]-48);
							tmp3=3;
							Register = 0x03;
						}
					}
					else if (RR_Buffer[bcnt]==59)
					{
						tmp3=3;
						Register=0x03;
					}
				}
			}
			bcnt++;
			TWCR_ACK;		// Send ACK - mybe move up bevore reading due to PasPi HW Bug????
			break;
			
		/* SLA+R received, ACK returned -> send data */
		case TW_ST_SLA_ACK:			//0xA8:
		bcnt1 = 0;
		bcnt=0;
		if ( ((AcDispComm>0) & (AcDispComm<5)) )		// RocRail TextSettings Display 1..8 - RNDisplay 0..3 :)
			SetActiveDisplay(AcDispComm-1,false);
		// NO BREAK !!!!! 
		Led_Rt_On;
		case TW_ST_DATA_ACK:
			Led_Rt_On;
			
			data=0;
			// Code Answer
			if ((Register&0x02)==2)
			{
				switch (intReg)
				{
					case Reg_ActiveDisplay:
						data=ActiveDisplay;
						intReg=0;
						Register=0;
						break;
					case Reg_ShowClock:
						data=DisSetting[ActiveDisplay].ShowClock;
						//data=GetDisplayClock(ActiveDisplay);
						intReg=0;
						Register=0;
						break;
					case Reg_ClockSide:
						data=DisSetting[ActiveDisplay].ClockSide;
						//data=GetClockSide(ActiveDisplay);
						intReg=Reg_ShowClock;	
						break;
					case Reg_Invert:
						data=DisSetting[ActiveDisplay].DispInvers;
						//data=GetDisplayInvert(ActiveDisplay);
						intReg=0;
						Register=0;
						break;
					case Reg_DisplayCount:
						data=ConDisplays;
						//data=GetDisplayCount();
						intReg=0;
						Register=0;
						RR_BuffPt=0;
						break;
					case  Reg_SyncDisplays:
						data=DisSetting[ActiveDisplay].DispSync;
						//data=GetSyncDisplays(ActiveDisplay);
						intReg=0;
						Register=0;
						break;
					case  Reg_ShowDepCol:
						data=DisSetting[ActiveDisplay].ShowDepCol;
						//data=GetShowDepartureCol(ActiveDisplay);
						intReg=0;
						Register=0;
						break;
					case Reg_DepColWidth:
						data=DisSetting[ActiveDisplay].DepColWidth;
						//data=GetDepartureCol(ActiveDisplay);
						intReg=0;
						Register=0;
						break;
					case Reg_Rotate:
						data=DisSetting[ActiveDisplay].Rotate;
						//data=getRotation();
						intReg=0;
						Register=0;
						break;
						
					case Reg_Blinktime:
						data=DisSetting[ActiveDisplay].BlinkTime;
						//data=GetBlinkTime(ActiveDisplay);
						intReg=0;
						Register=0;
						break;
					
					case  Reg_Contrast:
						switch (bcnt1)
						{
							case 0:
								tmp1 = DisSetting[ActiveDisplay].DispContrast;
								//tmp1 = GetKontrast(ActiveDisplay);
								tmp2 = tmp1/100;
								data = tmp2+48;
								break;
							case 1:
								tmp2 = (tmp1-(tmp2*100))/10;
								data = tmp2+48;
								break;
							case 2:
								tmp2 = (tmp1-(tmp2*10));
								data = tmp2+48;
								intReg=0;
								Register=0;
								tmp1=0;
								tmp2=0;
								tmp3=0;
								break;
						}
						break;
					case  Reg_SmallBmp:
						if (tmp3==3)
						{
							if (bcnt1==0)
							{
								//memset((void*)RR_Buffer,0,sizeof(RR_Buffer));
								//memset(RamBuffer,0,sizeof(RamBuffer));  // Not needed here - is in Function included.
								Fram_ReadSmallBMP(tmp1);
								data=RamBufferSize;
							}

							else if (bcnt1<20)
							data=RamBuffer[bcnt1-1];
							
							else if (bcnt1==20)
							{
								data=RamBuffer[bcnt1-1];
								intReg=0;
								Register=0;
								tmp1=0;
								tmp2=0;
								tmp3=0;
								
								bcnt1=0;
							}
						}
						break;
					case Reg_Font:
						if (tmp3==5)
						{
							if (bcnt1==0)
							{
								//memset((void*)RR_Buffer,0,sizeof(RR_Buffer));
								//memset(RamBuffer,0,sizeof(RamBuffer));	// Not needed here is in Function included.
								if (!Fram_ReadFont(tmp2,tmp1)) Led_Ge_Off;
								data=RamBufferSize;
							}
							else if (bcnt1<6)
							data=RamBuffer[bcnt1-1];
							
							else if (bcnt1==6)
							{
								data=RamBuffer[bcnt1-1];
								intReg=0;
								Register=0;
								tmp1=0;
								tmp2=0;
								tmp3=0;
								bcnt1=0;
								bcnt=0;
							}
						}
						break;
					case Reg_TextTemp:
						if (tmp3==3)
						{
							if (bcnt1==0)
							{
								//memset((void*)RR_Buffer,0,sizeof(RR_Buffer));
								//memset(RamBuffer,0,sizeof(RamBuffer));
								Fram_LoadTxt(tmp1);
								data=RamBufferSize;
							}

							else if (bcnt1<30)
							data=RamBuffer[bcnt1-1];
							
							else if (bcnt1==30)
							{
								data=RamBuffer[bcnt1-1];
								intReg=0;
								Register=0;
								tmp1=0;
								tmp2=0;
								tmp3=0;
								
								bcnt1=0;
							}
						}
						break;
				}
			}
			
			bcnt1++;
			TWDR = data;
			TWCR_ACK;
			//memset((void*)RR_Buffer,0,sizeof(RR_Buffer));  // ToDo: Check if here really needed - also done @I2C Start rec.
			
			data=0;
			break;
			
		case TW_ST_DATA_NACK: 						// 0xC0 No more Datas wanted
		case TW_SR_DATA_NACK: 						// 0x88 
		case TW_ST_LAST_DATA: 						// 0xC8  Last data byte in TWDR has been transmitted (TWEA = �0�); ACK has been received
		case TW_SR_STOP:
		case TW_NO_INFO:							// 0xA0 STOP empfangen
		case TW_BUS_ERROR:
		default:
			Led_Rt_Off;
			//BlinkTimerStart();
			//if ((bcnt>0)&((Register&0x02)==0)) RR_BuffPt=bcnt;
			if ((bcnt>1)&(intReg==0)) RR_BuffPt=bcnt;    // Only if intReg = 0 set BufferCount - otherwise it will be parsed even if no datas are here to pharse....
			memset(RamBuffer,0,sizeof(RamBuffer));
			TWCR_RESET;				// End the I2C communication - Reset I2C to listen.
			bcnt = 0;						
			bcnt1=0;
		break;	
	}
	sei();
	//BlinkTimerStart();
}

ISR(TIMER1_OVF_vect)	
{
	
	/* restart timer */
	TCNT1 = 0xE17B;
	uint8_t TempAct = ActiveDisplay;
	for (uint8_t i = 0; i<4;i++)
	{
		if ((((ConDisplays>>i)&0x01) == 1))
		{
			SetActiveDisplay(i,true);
			if ((((BlinkCnt[ActiveDisplay])==0) && ((DisSetting[ActiveDisplay].BlinkState>>7)==1) ))
			{
				DoBlink();
				//BlinkCnt[ActiveDisplay]=GetBlinkTime(ActiveDisplay);
				BlinkCnt[ActiveDisplay]=DisSetting[ActiveDisplay].BlinkTime;
			}
			if ((DisSetting[ActiveDisplay].BlinkState>>7)==1)
				BlinkCnt[ActiveDisplay]--;
		}
	}
	SetActiveDisplay(TempAct,true);
	
}

// Looks if the Display q should be synchronized with tmpAct or also if q = tmpAct
bool AskWrite(uint8_t q,uint8_t tmpAct,bool doSetMux)
{
	if (DisSetting[tmpAct].DispSync>0)
	{
		if ((((DisSetting[tmpAct].DispSync>>q)&0x01)==1) | (q==tmpAct))
		{
			SetActiveDisplay(q,doSetMux);
			return true;
		}
	}
	else if ((q==tmpAct))
	{
		SetActiveDisplay(q,doSetMux);
		return true;
	}
	return false;
}

// Looks if the Setup parameters should also be synchronized with tmpAct
bool AskSWrite (uint8_t q, uint8_t tmpAct)
{
		if (((((DisSetting[tmpAct].DispSync>>q)&0x01)==1) & (SetupCon==1)) | (q==tmpAct))
		{
			SetActiveDisplay(q,false);
			return true;
		}
		return false;
}

// Pharsing of the received String.
static inline uint8_t PharseText(volatile uint8_t *TxtIn,volatile uint8_t Len)
{	
	uint8_t ComReg=0;
	
	uint8_t m,h,xtmp,Nr,rbtcnt,CharNr,FontNr = 0;
	uint8_t iVal[2] = {0,0};
	
	if ( ((AcDispComm>0) & (AcDispComm<5)) )		// RocRail TextSettings Display 1..8 - RNDisplay 0..3 :)
	{
		SetActiveDisplay(AcDispComm-1,true);
	}
	
	uint8_t TempDis = ActiveDisplay; // Yes right here bevore we set the active Display...
	
	if (intReg!=0) return 0;   // intReg is !=0 then assume we are in middle of an I2C communication or the last comm was not finished with Stop sign - exit....
	
	
		   
	for (uint8_t i = 0;i < Len;i++)
	{
		xtmp=0;
		m=0;
		h=0;
		
		// No Command - Print Text or start Command
		if (ComReg==0)
		{
			if (TxtIn[i]==123)							// Start Command
				{
					ComReg=1;
				}
			else
				{
					for (uint8_t d = 0;d<4; d++)
					{
						if (AskWrite(d,TempDis,false))
						{
							if (((DisSetting[d].BlinkState>>2)&0x01)==0)
								write(TxtIn[i]);
							if ((DisSetting[d].BlinkState&0x02)==0x02)
								WriteAlternate(TxtIn[i],(DisSetting[d].BlinkState&0x01));
						}
					}
				}
		}
		else								// Is an Action Command - no Textwrite
		{
			switch (TxtIn[i])
			{
				// End of Command
				case 125:	
					ComReg=0;
					break;
					
				// Write { - is in Action Command
				case  123:  //123:
					for (uint8_t d = 0;d<4; d++)
					{
						if (AskWrite(d,TempDis,false))
						{
							if (((DisSetting[d].BlinkState>>2)&0x01)==0)
								write(123);
							if ((DisSetting[d].BlinkState&0x02)==0x02)
							WriteAlternate(123,(DisSetting[d].BlinkState&0x01));
						}
					}
					break;
				// Set Time
				case 'C':
					// Hour 1 or 2 Digit
					if (TxtIn[i+2]==':')	//One Digit
					{
						i++;
						h=TxtIn[i]-48;
					}
					else
					{
						i++;
						h=(TxtIn[i]-48)*10;
						i++;
						h+=(TxtIn[i]-48);
					}
					i++;
					if (TxtIn[i+2]==125)	//One Digit
					{
						i++;
						m=TxtIn[i]-48;
					}
					else
					{
						i++;
						m=(TxtIn[i]-48)*10;
						i++;
						m+=TxtIn[i]-48;
					}
					SetClock(h,m,true);
					break;
					
				// Set Line
				case 'L':
					i++;
					for (uint8_t d = 0;d<4; d++)
					{
						if (AskWrite(d,TempDis,false))
						{
							SetActiveLine(TxtIn[i]-48);
							if ((DisSetting[d].BlinkState&0x02)==0x02)
								BlinkEnd();
						}
					}
					break;	
				
				// Set Column
				case 'T':
					i++;
					for (uint8_t d = 0;d<4; d++)
					{
						if (AskWrite(d,TempDis,false))
						SetActiveSection(TxtIn[i]-48);
					}
					break;
					
				// Set Font
				case 'F':
					i++;
					for (uint8_t d = 0;d<4; d++)
					{
						if (AskWrite(d,TempDis,false))
						SetFont(TxtIn[i]-48);
					}
					break;
					
				// Display Bitmap 
				case 'B':
					i++;
					// ASCII TO INT
					if ((TxtIn[i+1]>47) & (TxtIn[i+1]<58))  // 2 DIGIT
					{
						xtmp = (TxtIn[i]-48)*10;
						i++;
						xtmp +=TxtIn[i]-48;
					}
					else
					{
						xtmp = TxtIn[i]-48;
					}
					for (uint8_t d = 0;d<4; d++)
					{
						if (AskWrite(d,TempDis,false))
						{
							if (((DisSetting[d].BlinkState>>2)&0x01)==0)
								writeBMP(xtmp);
							if ((DisSetting[d].BlinkState&0x02)==0x02)
								AlternateBMP(xtmp,(DisSetting[d].BlinkState&0x01));
						}
					}
					break;
					
				// Clear DisplayBuffer
				case 'E':
					for (uint8_t d = 0;d<4; d++)
					{
						if (AskWrite(d,TempDis,false))
						Dis_clearDisplay();
					}
					break;
					
				// Send DisplayBuffer to Display
				case 'P':
					for (uint8_t d = 0;d<4; d++)
					{
						if (AskWrite(d,TempDis,true))
						{
							if ((DisSetting[d].BlinkState&0x02)==0x02)
								BlinkEnd();
							Dis_display(false);
						}
							
					}
					break;
					
				// Text Templates "GL" & "GS"
				case 'G':
					if (TxtIn[i+1]=='L')
					{
						i+=2;
						uint8_t tmpTxtLen=0;
						uint8_t tmpTxtBuff[30];
						uint8_t xtmp=0;
						// ASCII TO INT
						if ((TxtIn[i+1]>47) & (TxtIn[i+1]<58))  // 2 DIGIT
						{
							xtmp = (TxtIn[i]-48)*10;
							i++;
							xtmp +=TxtIn[i]-48;
						}
						else
						{
							xtmp = TxtIn[i]-48;
						}
						Fram_LoadTxt(xtmp);
						tmpTxtLen=RamBufferSize;
						memcpy(tmpTxtBuff,RamBuffer,RamBufferSize);
						SetActiveDisplay(TempDis,true);
						PharseText((volatile uint8_t*)tmpTxtBuff,(volatile uint8_t) tmpTxtLen);
					}
					else if (TxtIn[i+1]=='S')
					{
						i+=2;
						// Save Text to NVRam
						uint8_t tmpTxtBuff[31];
						uint8_t xtmp=0;
						memset(tmpTxtBuff,0,31);
						// ASCII TO INT
						if ((TxtIn[i+1]>47) & (TxtIn[i+1]<58))  // 2 DIGIT
						{
							xtmp = (TxtIn[i]-48)*10;
							i++;
							xtmp +=TxtIn[i]-48;
						}
						else
						{
							xtmp = TxtIn[i]-48;
						}
						i++;
						if (TxtIn[i]=='\\')  // {GS0\Wien\}
						{
							i++;
							tmpTxtBuff[0]=0;
							while ((TxtIn[i]!='\\') & (i<Len) )
							{
								if (tmpTxtBuff[0]<31)
								{
									tmpTxtBuff[tmpTxtBuff[0]+1]=TxtIn[i];
									tmpTxtBuff[0]++;
								}
								i++;
							}
							
							Fram_SaveTxt(tmpTxtBuff,xtmp);
						}
					}
					break;
				
				// Blink Command
				case 'J':
					i++;
					
					SetActiveDisplay(TempDis,true);
					switch (TxtIn[i])
					{
						case '0':
							for (uint8_t d=0;d<4;d++)
							{
								
								if (AskWrite(d,TempDis,false))
								{
									BlinkEnd();
								}
							}
						break;
						
						case '1':
							rbtcnt = i;
							for (uint8_t d=0;d<4;d++)
							{
								
								if (AskWrite(d,TempDis,false))
								{
									BlinkStart();
								}
							}
						break;
						
						case '2':
							for (uint8_t d=0;d<4;d++)
							{
								
								if (AskWrite(d,TempDis,false))
								{
									DisSetting[d].BlinkState|=0x01;
									DisSetting[d].BlinkState|=(1<<2);
								}
							}	
							
						break;
								/*
									i=rbtcnt;
									for (Nr=0;Nr<2;Nr++)
									{
										if (TxtIn[i+1]=='[') // Alternativtext
										{
											i+=2;
											while ((TxtIn[i]!=']') & (i<Len) & (TxtIn[i]!=125) )
											{
												
												WriteAlternate(TxtIn[i],Nr);
												if (Nr==0) write(TxtIn[i]);	
												i++;
											}
											if (Nr==1) BlinkEnd();
										}
										else if (TxtIn[i+1]=='#')	// Alternativ Bild
										{
											i+=2;
											if ( ((TxtIn[i+1] > 47) & (TxtIn[i+1] < 58)) )
											{
												xtmp = (TxtIn[i]-48)*10 + (TxtIn[i+1]-48);
												i++;
											}
											else if ( ((TxtIn[i] > 47) & (TxtIn[i] < 58)) )
											{
												xtmp = TxtIn[i]-48;
											}
											AlternateBMP(xtmp,Nr);
											if (Nr==0) writeBMP(xtmp);
											if (Nr==1)
											{
												BlinkEnd();
											}
									
										}
									}
								}
							}
							*/
						//break;
						
						case 'S':
							i++;
							SetBlinkTime(TxtIn[i]-48,TempDis);
						break;
					}
					
					break; 
				// SETUP Commands
				//
				// Set Setup also for sync Displays
				case 'K':
					i++;
					if ((TxtIn[i]-48)==1)
					SetupCon=1;
					else
					SetupCon=0;
					break;
				
				// SetClockDisplay settings
				case 'S':
					i++;
					if ( ((TxtIn[i]=='L') | (TxtIn[i]=='R')) ) xtmp = 1;
					if ( ((TxtIn[i] > 47) & (TxtIn[i] < 58)) ) xtmp = 2;
					for (uint8_t d=0;d<4;d++)
					{
						if (AskSWrite(d,TempDis))
						{
							if (xtmp==1) SetClockSide(TxtIn[i],d);
							if (xtmp==2) SetDisplayClock(TxtIn[i]-48,d);
						}
					}
					break;
					
				// Show Departure Column
				case 'D':
					i++;
					for (uint8_t d=0;d<4;d++)
					{
						if (AskSWrite(d,TempDis))
						{
							SetShowDepartureCol(TxtIn[i]-48,d);
						}
					}
					break;
					
				// Departure Column width
				case 'W':
					i++;
					// ASCII TO INT
					if ((TxtIn[i+1]>47) & (TxtIn[i+1]<58))  // 2 DIGIT
					{
						xtmp = (TxtIn[i]-48)*10;
						i++;
						xtmp +=TxtIn[i]-48;
					}
					else
					{
						xtmp = TxtIn[i]-48;
					}
						
					for (uint8_t d=0;d<4;d++)
					{
						if (AskSWrite(d,TempDis))
						{
							SetDepartureCol(xtmp,d);
						}
					}
					break;
					
				// Invert Settings
				case 'I':
					xtmp=0;
					i++;
					// ASCII TO INT
					if ((TxtIn[i+2]>47) & (TxtIn[i+2]<58))  // 3 DIGIT
					{
						xtmp = (TxtIn[i]-48)*100;
						//i++;
						xtmp +=(TxtIn[i+1]-48)*10;
						//i++;
						xtmp +=(TxtIn[i+2]-48);
						i+=2;
					}
					else if ((TxtIn[i+1]>47) & (TxtIn[i+1]<58))  // 2 DIGIT
					{
						xtmp = (TxtIn[i]-48)*10;
						i++;
						xtmp +=TxtIn[i]-48;
							
					}
					else
					{
						xtmp = TxtIn[i]-48;
					}
					//SetActiveDisplay(TempDis,false);
					for (uint8_t d=0;d<4;d++)
					{
						if (AskSWrite(d,TempDis))
						{
							SetDisplayInvert(xtmp,d);
						}
					}
					break;
					
				// Set Active Display
				case 'A':
					i++;
					SetActiveDisplay(TxtIn[i]-48,true);
					TempDis = TxtIn[i]-48;
					break;
					
				// Set Rotation
				case 'R':
					xtmp=0;
					i++;
					// ASCII TO INT
					if (( ((TxtIn[i+2]>47) & (TxtIn[i+2]<58)) & ((TxtIn[i+1]>47) & (TxtIn[i+1]<58)) ))  // 3 DIGIT
					{
						xtmp = (TxtIn[i]-48)*100;
						xtmp +=(TxtIn[i+1]-48)*10;
						xtmp +=(TxtIn[i+2]-48);
						i+=2;
					}
					else if ((TxtIn[i+1]>47) & (TxtIn[i+1]<58))  // 2 DIGIT
					{
						xtmp = (TxtIn[i]-48)*10;
						i++;
						xtmp +=TxtIn[i]-48;
					}
					else
					{
						xtmp = TxtIn[i]-48;
					}
					SetActiveDisplay(TempDis,true);
					setRotation(xtmp,ActiveDisplay);
					break;
					
				// Set Sync Displays
				case 'V':
					xtmp=0;
					i++;
						
					if ((TxtIn[i+1]>47) & (TxtIn[i+1]<58))  // 2 DIGIT
					{
						xtmp = (TxtIn[i]-48)*10;
						i++;
						xtmp +=TxtIn[i]-48;
					}
					else
					{
						xtmp = TxtIn[i]-48;
					}
					SetActiveDisplay(TempDis,false);
					SetSyncDisplays(xtmp);
					break;
					
				// Set Connected Displays
				case 'Z':
					xtmp=0;
					i++;
					// ASCII TO INT
					if ((TxtIn[i+1]>47) & (TxtIn[i+1]<58))  // 2 DIGIT
					{
						xtmp = (TxtIn[i]-48)*10;
						i++;
						xtmp +=TxtIn[i]-48;
					}
					else
					{
						xtmp = TxtIn[i]-48;
					}
					SetActiveDisplay(TempDis,false);
					SetDisplayCount(xtmp);
					break;
					
				// Set Contrast
				case 'H':
					xtmp=0;
					i++;
					// ASCII TO INT
					if (( ((TxtIn[i+2]>47) & (TxtIn[i+2]<58)) & ((TxtIn[i+1]>47) & (TxtIn[i+1]<58)) ))  // 3 DIGIT
					{
						xtmp = (TxtIn[i]-48)*100;
						xtmp +=(TxtIn[i+1]-48)*10;
						xtmp +=(TxtIn[i+2]-48);
						i+=2;
					}
					else if ((TxtIn[i+1]>47) & (TxtIn[i+1]<58))  // 2 DIGIT
					{
						xtmp = (TxtIn[i]-48)*10;
						i++;
						xtmp +=TxtIn[i]-48;
					}
					else
					{
						xtmp = TxtIn[i]-48;
					}
					SetActiveDisplay(TempDis,true);
					SetKontrast(xtmp,ActiveDisplay);
					break;
					
				// Set Cursor X
				case 'X':
					xtmp=0;
					i++;
					// ASCII TO INT
					if ((TxtIn[i+1]>47) & (TxtIn[i+1]<58))  // 2 DIGIT
					{
						xtmp = (TxtIn[i]-48)*10;
						i++;
						xtmp +=TxtIn[i]-48;
					}
					else
					{
						xtmp = TxtIn[i]-48;
					}
					for (uint8_t d = 0;d<4; d++)
					{
						if (AskWrite(d,TempDis,false))
						SetCsr_X(xtmp);
					}
					break;
					
				// Set Cursor Y
				case 'Y':
					xtmp=0;
					i++;
					// ASCII TO INT
					if ((TxtIn[i+1]>47) & (TxtIn[i+1]<58))  // 2 DIGIT
					{
						xtmp = (TxtIn[i]-48)*10;
						i++;
						xtmp +=TxtIn[i]-48;
					}
					else
					{
						xtmp = TxtIn[i]-48;
					}
					for (uint8_t d = 0;d<4; d++)
					{
						if (AskWrite(d,TempDis,false))
						SetCsr_Y(xtmp);
					}
					break;
				
				// Reset Display
				case 'U':
					i++;
					if ((TxtIn[i]>47) & (TxtIn[i]<58))
					{
						StartSingleDisplay(TxtIn[i]-48);
					}
					else
					{
						Dis_StartDisplay();
					}
					break;
					
				// Turn Display Off
				case 'O':
					i++;
					if ((TxtIn[i]>47) & (TxtIn[i]<58))
					{
						TurnOffDisplay(TxtIn[i]-48);
					}
					else
					{
						TurnOffAllDisplays();
					}
					break;
				
				// Write Bitmap to Memory
				case 'M':
					memset(iVal,0,2);
					// BMP NR
					i++;
					memcpy(iVal, (uint8_t*)RR_Buffer+i,2);
					Nr = hex2num(iVal,2);
					i+=2;
					// ;
					if (RR_Buffer[i]!=';')
					{
						SetActiveDisplay(TempDis,true);
						return 0;
					}
					i++;
					// BMP Len = Index 0 in RamBuffer and RamBufferSize
					rbtcnt = 1;
					memset(RamBuffer,0,sizeof(RamBuffer));
					memcpy(iVal, (uint8_t*)RR_Buffer+i,2);
					RamBufferSize=hex2num(iVal,2);
					i+=2;
					if (RR_Buffer[i]!=';')
					{
						SetActiveDisplay(TempDis,true);
						return 0;
					}
					i++;
					while ((i<Len)&(rbtcnt<21)&(RR_Buffer[i]!=125))
					{
						memcpy(iVal, (uint8_t*)RR_Buffer+i,2);
						RamBuffer[rbtcnt]=hex2num(iVal,2);
						i+=2;
						if (RR_Buffer[i]!=';')
						{
							if (RR_Buffer[i]!=125)
							{
								SetActiveDisplay(TempDis,true);
								return 0;
							}
							else
								i--;
						}
						i++;
						rbtcnt++;
					}
					//if (RR_Buffer[i]==125) i--;
					if (RR_Buffer[i]==125) i--;
					if (rbtcnt<RamBufferSize) //??
					{
						SetActiveDisplay(TempDis,true);
						return 0;
					}
					RamBuffer[0]=RamBufferSize;
					Fram_WriteSmallBMP(Nr);
					break;
					
				// Write Font char to Memory
				case 'N':
					memset(iVal,0,2);
						
					i++;
					// FontNr
					memcpy(iVal, (uint8_t*)RR_Buffer+i,2);
					FontNr = hex2num(iVal,2);
					i+=2;
					// ;
					if (RR_Buffer[i]!=';')
					{
						SetActiveDisplay(TempDis,true);
						return 0;
					}
					i++;
					//CharNr
					memcpy(iVal, (uint8_t*)RR_Buffer+i,2);
					CharNr = hex2num(iVal,2);
					i+=2;
					// ;
					if (RR_Buffer[i]!=';')
					{
						SetActiveDisplay(TempDis,true);
						return 0;
					}
					i++;

					// Font Len = Index 0 in RamBuffer and RamBufferSize
					rbtcnt = 1;
					memset(RamBuffer,0,sizeof(RamBuffer));
					memcpy(iVal, (uint8_t*)RR_Buffer+i,2);
					RamBufferSize=hex2num(iVal,2);
					i+=2;
					if (RR_Buffer[i]!=';')
					{
						SetActiveDisplay(TempDis,true);
						return 0;
					}
					i++;
					//while ((i<Len)&(rbtcnt<7)&(RR_Buffer[i]!=125))
					while ((i<Len)&(rbtcnt<7)&(RR_Buffer[i]!=125))
					{ 
						memcpy(iVal, (uint8_t*)RR_Buffer+i,2);
						RamBuffer[rbtcnt]=hex2num(iVal,2);
						i+=2;
						if (RR_Buffer[i]!=';')
							{
							//if (RR_Buffer[i]!=125)
							if (RR_Buffer[i]!=125)
							{
								SetActiveDisplay(TempDis,true);
								return 0;
							}
							else
								i--;
						}
						i++;
						rbtcnt++;
					}
					//if (RR_Buffer[i]==125) i--;
					if (RR_Buffer[i]==125) i--;
					if (rbtcnt<RamBufferSize)
					{
						SetActiveDisplay(TempDis,false);
						return 0;
					}
					RamBuffer[0]=RamBufferSize;
					Fram_WriteFont(CharNr,FontNr);
					break;
					
				// Factory Reset
				case '!':
					//if ((TxtIn[i+1]=='*') & (TxtIn[i+2]=='!') & (TxtIn[i+4]==125))
					if ((TxtIn[i+1]=='*') & (TxtIn[i+2]=='!') & (TxtIn[i+4]==125))
					Led_Ge_Off;

					if (SetFactoryReset(TxtIn[i+3]-48))
					{
						TurnOffAllDisplays();
						start();
					}
					break;	
					
				// Start Bootloader
				case '2':
					Led_Ge_Off;
					//if ((TxtIn[i+1]=='B') & (TxtIn[i+2]=='O') & (TxtIn[i+3]=='O') & (TxtIn[i+4]=='T') & (TxtIn[i+5]==125) )
					if ((TxtIn[i+1]=='B') & (TxtIn[i+2]=='O') & (TxtIn[i+3]=='O') & (TxtIn[i+4]=='T') & (TxtIn[i+5]==125) )
					{
						TurnOffAllDisplays();
						bootloader();
					}
					break;				
			}
			
		}
	}
	SetActiveDisplay(TempDis,true);
return 0x00;
}

int main(void)
{
	TWI_Display();
	// Reset Display via Reset wire
	RST_INI;
	RST_SET;
	_delay_ms(1);
	RST_CLR;
	_delay_ms(10);
	RST_SET;
	_delay_ms(2);
	
	// Basis I2C Adresse
	uint8_t RnAddress = 0b01010000;
	
	
	
	//Set in and outputs
	// Led Yellow and Red
	DDRB |= ((1<<PORTB1) | (1<<PORTB2));
	Led_Ge_On;
	
	// Hardware TWI INIT
	// Set Address Pins Input Read 4 Low bit of Address
	// 7Bit = 50 - 5F
	// 8Bit = C0 - DE
	DDRC &= ~((1 << DDC0) | (1 << DDC1) | (1 << DDC2) | (1 << DDC3));	
	RnAddress |= (PINC&0x0F);
	// Set Address with Jumpers (8Bit Address)
	TWAR=((0x50|(PINC & 0x0F))<<1);

	TWCR &= ~(1<<TWSTA)|(1<<TWSTO);
	TWCR|= (1<<TWEA) | (1<<TWEN)|(1<<TWIE);
	
	memset((void*)RR_Buffer,0x00,sizeof(RR_Buffer));
	RR_BuffPt=0;
			
	sei();
	StartSoftI2C();
	
	memset(RamBuffer,0,sizeof(RamBuffer));
	
	
	// Are Settings saved in F-Ram? Last F-Ram Byte = 128 then OK if != 128 then transfer Initialsettings, Fonts, Bitmaps from Flash into F-Ram (first Boot)
	if (Fram_ReadByte(EE_FirstStart)!=128)
	{
		SetFactoryReset(7);
	}
	// Lade Display settings - 
	GetSettings();
					// Setup Display0 for Test
					/*
					SetDisplaySize(96,16);
					SetDisplayCount(1);
					setRotation(2,0);
					SetActiveDisplay(0);
					SetActiveLine(0);
					SetActiveSection(0);
					SetShowDepartureCol(1,0);
					SetDepartureCol(19,0);
					SetDisplayClock(1,0);
					SetClockSide('R',0);
					*/
	// Init Displays
	Dis_StartDisplay();
	
	// Loop as long as 1 is True or 0 is False ;)
    while(1)
    {
		// If I2C BufferCount = 0 then sit and wait ....
		// Count will be set from ISR at the end of transmission - to keep the ISR as short as possible
		if (RR_BuffPt>0)		
		{
			Led_Ge_Tgl;
			// So lange was zum arbeiten da ist darf nix neues kommen sonst gibts ein durcheinander - Hint an Manager: so sollte es auch bei HumanRecources sein ;)
			TWCR_NACK;
			// Hey you have to work, .....
			BlinkTimerStop();
			PharseText(RR_Buffer,RR_BuffPt);
			BlinkTimerStart();
			TWCR_ACK;
			Led_Ge_Tgl;
			//Got finished !!
			RR_BuffPt=0;
		}
    }
}

