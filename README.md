﻿RocDisplay Firmware

written in AtmelStudio 6.1

Content:
Bootloader and RocDisplay Firmware

Bootloader:
    Version:    1.11
    I2C Bootloader with Intel Hex File parser.
    Fuses for Bootloadersize: 2048 words (4k)
    AT Mega fuses:
        * Fuse High:    0xd1 (2048 words bootloader execute bootloader after reset)  / 0xd0 (2048 words no bootloader execute after Reset)
        * Fuse Low:     0xdf (16Mhz external RC-Osz.)
        * Fuse E:       0xfd (2.7V BOD)
    Projectfile: RocDisplayBootloader.cproj
    Hex file: ./Debug/RocDisplayBootloader.hex
	
	
RocDisplay:
    Version 1.0
    Firmware for RocDisplay.
    Circut and Board layout for Hardware

Changes:
    RocDisplay:
	    07.10.2015 - Bugfix for RasPi I2C HW Bug.